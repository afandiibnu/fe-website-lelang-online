const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      container: {
        center: true,
      },
      spacing: {
        hero: "600px",
      },
      colors: {
        primary: "#315AFE",
        "primary-hover": "#1B48FF",
        "secondary-purple": "#E6EAFF",
        secondary: "#EDF1FF",
        black: "#171717",
        "dark-blue": "#121926",
        "dark-blue-secondary": "#1E293D",
        "color-secondary": "#888888",
        "light-green": "#F2FE66",
      },
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
      },
      backgroudImage: {
        pattern: "url('./public/pattern.png')",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  important: "#tailwind-selector",
};
