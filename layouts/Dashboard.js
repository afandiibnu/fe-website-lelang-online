import Head from "next/head";
import React, { useEffect, useState } from "react";
import Sidebar from "../components/Sidebar";
import Title from "../components/Title";
import useAuthenticationPage from "../helper/authenticationPage";
import { parseJwt } from "../helper/DecodeJwt";
import { useRouter } from "next/router";
import { userRepository } from "../repository/user";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ChevronDown } from "akar-icons";
import { mutate } from "swr";

const Dashboard = ({ title = "Aero auction", children }) => {
  useAuthenticationPage();
  const router = useRouter();
  const [user, setUser] = useState([]);
  const [isOpen, setIsOpen] = useState(false);

  let token;
  const logoutHandler = async () => {
    localStorage?.removeItem("token");
    mutate(router?.push("/"));
    router?.reload();
  };

  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }

  useEffect(() => {
    const decodeJwt = parseJwt(token);

    if (decodeJwt?.result.account.role.description !== "Admin") {
      router?.push("/");
    }
  });

  const { data: userData } = userRepository.hooks.getUserDetail();

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData.data);
    }
  }, [userData]);

  return (
    <div className=" bg-secondary">
      <Head>
        <title>{title}</title>
      </Head>
      <div className="flex text-black box-content">
        <Sidebar />
        <div className="flex flex-col w-5/6 p-10">
          <div className="flex justify-between items-center">
            <div className="">
              <Title title={title} size="text-xl" color="primary" />
            </div>
            <div className=" rounded-md flex gap-4 bg-white px-6 py-5 shadow-xl shadow-primary/5">
              <Menu className="relative mr-0 z-10" as="div">
                <Menu.Button
                  as="div"
                  className={`text-sm font-medium  cursor-pointer flex items-center justify-between md:justify-start`}
                  onClick={() => setIsOpen(!isOpen)}
                >
                  <div className="flex items-center gap-x-4">
                    <p className="capitalize">{user?.name}</p>
                  </div>

                  <div
                    className={`${
                      isOpen ? "rotate-180" : "rotate-0"
                    } ml-2 transition duration-100 flex items-center gap-x-2`}
                  >
                    <ChevronDown size={14} strokeWidth={4} />
                  </div>
                </Menu.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="bg-white absolute right-0 mt-4 flex flex-col w-40 py-4 focus:outline-none drop-shadow-md rounded text-sm text-color-secondary ">
                    <Menu.Item
                      as={"div"}
                      className="w-full hover:bg-secondary rounded-sm cursor-pointer"
                    >
                      <button
                        onClick={logoutHandler}
                        className="w-full py-2 text-center"
                      >
                        Logout
                      </button>
                    </Menu.Item>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </div>
          {children}
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
