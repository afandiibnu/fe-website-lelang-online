import React from "react";
import Head from "next/head";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Banner from "../components/Banner";
import Image from "next/image";
import Sidebar from "../components/Profile/Sidebar";
import { useEffect, useState } from "react";
import { userRepository } from "../repository/user";
import useAuthenticationPage from "../helper/authenticationPage";
import { parseJwt } from "../helper/DecodeJwt";
import NavbarLogin from "../components/NavbarLogin";
import { mutate } from "swr";

const Layout = ({ title, children }) => {
  const [user, setUser] = useState(null);
  const [role, setRole] = useState("");
  const { data: userData } = userRepository.hooks.getUserDetail();
  mutate(userData);
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setIsLogin(true);
      // console.log(decodeJwt);
    } else {
      setIsLogin(false);
    }
  }, []);

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData?.data);
    }
  }, [userData]);
  // console.log(userData);
  const status = user?.status_of_auctioner;
  // console.log(status);

  useEffect(() => {
    if (status == false) {
      setRole("Bidder");
    } else if (status == true) {
      setRole("Auctioner");
    }
  }, [status]);

  useAuthenticationPage();
  return (
    <div className="min-h-screen">
      <Head>
        <title>{title}</title>
      </Head>
      {isLogin ? (
        <NavbarLogin
          color="bg-dark-blue"
          textColor="text-white"
          searchColor="bg-dark-blue-secondary"
          logoColor="Dark"
        />
      ) : (
        <Navbar
          color="bg-white"
          textColor="text-color-secondary"
          searchColor="bg-secondary"
          logoColor="Light"
        />
      )}
      <div className="drop-shadow-2xl shadow-primary">
        <Banner height="h-[200px]">
          <div className="mx-40 2xl:mx-56 h-full flex items-center gap-4">
            <div className="rounded-full overflow-hidden w-[100px] h-[100px]">
              <img
                src={`http://localhost:3222/users/get-profile-image/${user?.picture}`}
                width="100px"
                height="100px"
                className=""
              />
            </div>
            <p className="h1 text-4xl text-white font-bold capitalize">
              {user?.name}
            </p>
            <p className="capitalize text-sm font-medium bg-light-green px-6 py-2 rounded-full">
              {role}
            </p>
          </div>
        </Banner>
      </div>
      <div className="min-h-screen mx-40 2xl:mx-56 py-20 flex ">
        <div className="w-2/12">
          <Sidebar />
        </div>
        <div className="ml-20 w-full">{children}</div>
      </div>
      <Footer />
    </div>
  );
};

export default Layout;
