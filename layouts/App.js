import Head from "next/head";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import NavbarLogin from "../components/NavbarLogin";
import { parseJwt } from "../helper/DecodeJwt";
import { useState, useEffect } from "react";

export default function App({ title = "Aero Auction", children }) {
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setIsLogin(true);
      // console.log(decodeJwt);
    } else {
      setIsLogin(false);
    }
  }, []);

  return (
    <div className="min-h-screen">
      <Head>
        <title>{title}</title>
      </Head>
      {isLogin ? (
        <NavbarLogin
          color="bg-dark-blue"
          textColor="text-white"
          searchColor="bg-dark-blue-secondary"
          logoColor="Dark"
        />
      ) : (
        <Navbar
          color="bg-white"
          textColor="text-color-secondary"
          searchColor="bg-secondary"
          logoColor="Light"
        />
      )}
      <div className="mb-40">{children}</div>
      <Footer />
    </div>
  );
}
