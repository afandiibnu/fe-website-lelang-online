import Image from "next/image";
import Head from "next/head";
import Link from "next/link";

export default function Auth({ title = "Aero Auction", children }) {
  return (
    <div className="flex justify-center flex-col items-center min-h-screen bg-[url('/bg.png')]">
      <Head>
        <title>{title}</title>
      </Head>
      <Link href="/">
        <a className="mb-14">
          <Image src="/logoLight.svg" width="72" height="40" />
        </a>
      </Link>
      <div className="mb-14">{children}</div>
    </div>
  );
}
