import Head from "next/head";
import { useEffect, useState } from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import NavbarLogin from "../components/NavbarLogin";
import { parseJwt } from "../helper/DecodeJwt";
import { useRouter } from "next/router";

export default function Guest({ title = "Aero Auction", children }) {
  const [isLogin, setIsLogin] = useState(false);

  const router = useRouter();

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setIsLogin(true);
      if (decodeJwt?.result?.account?.role?.description == "Admin") {
        router.push("/dashboard");
        console.log("gue admin");
      }
    } else {
      setIsLogin(false);
    }
  }, []);

  return (
    <div className="min-h-screen bg-[url('/bg.png')]">
      <Head>
        <title>{title}</title>
      </Head>
      {isLogin ? (
        <div>
          <NavbarLogin
            color="bg-dark-blue"
            textColor="text-white"
            searchColor="bg-dark-blue-secondary"
            logoColor="Dark"
          />
        </div>
      ) : (
        <div className="sticky">
          <Navbar
            color="bg-white"
            textColor="text-color-secondary"
            searchColor="bg-secondary"
            logoColor="Light"
          />
        </div>
      )}
      <div>{children}</div>
      <Footer />
    </div>
  );
}
