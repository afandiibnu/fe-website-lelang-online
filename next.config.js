const withAntdLess = require("next-plugin-antd-less");
const antdVariables = require("./styles/antd_variables");

module.exports = {
  async rewrites() {
    return [
      {
        source: "/login",
        destination: "/auth/login",
      },
      {
        source: "/register",
        destination: "/auth/register",
      },
    ];
  },
};
