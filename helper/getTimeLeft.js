const getTimeLeft = (enddate, unit) => {
  const now = new Date();
  const end_date = enddate;

  const timeLeft = end_date.getTime() - now.getTime();

  let result;

  switch (unit) {
    case "days":
      result = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
      break;
    case "hours":
      result = Math.floor(
        (timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      break;
    case "minutes":
      result = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
    default:
      break;
  }

  return result;
};

export default getTimeLeft;
