export const getHighestBid = (data) => {
  const bid = [];

  data.map((d) => bid.push(d.price_bid));

  const highestBid = Math.max.apply(Math, bid);

  return highestBid;
};
