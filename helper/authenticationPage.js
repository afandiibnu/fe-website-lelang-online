import { useRouter } from "next/router";
import { parseJwt } from "./DecodeJwt";

const useAuthenticationPage = () => {
  const router = useRouter();

  if (typeof window !== "undefined") {
    if (!localStorage.getItem("token")) {
      window.location.href = "/login";
    }
  }
};

export default useAuthenticationPage;
