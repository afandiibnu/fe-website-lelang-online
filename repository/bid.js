import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  getbyProduct: (id) => `/logbid/get/${id}`,
  getCountBidder: (id) => `/logbid/get/people/${id}`,
  getHighestBid: (id) => `/logbid/hight/${id}`,
  createLogBid: (id) => `/logbid/create/logbid/${id}`,
};

const hooks = {
  getLogBid(id) {
    return useSWR(url.getbyProduct(id), http.fetcher);
  },
  getCountBidder(id) {
    return useSWR(url.getCountBidder(id), http.fetcher);
  },
  getHighestBid(id) {
    return useSWR(url.getHighestBid(id), http.fetcher);
  },
};

const manipulateData = {
  createLogBid(id, data) {
    return http.post(url.createLogBid(id)).send(data);
  },
};

export const bidRepository = {
  url,
  manipulateData,
  hooks,
};
