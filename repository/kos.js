import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  kost: ({ page, pageSize }) => `/kos?page=${page}&pageSize=${pageSize}`,
  createKost: () => `/kos/`,
};

const hooks = {
  useKost(filter) {
    return useSWR(url.kost(filter), http.fetcher);
  },
};

const manipulateData = {
  createKos(data) {
    return http.post(url.createKost()).send(data);
  },
  uploadProductImage(data) {
    return http.upload(url.uploadProductImage(), data);
  },
};

export const kosRepository = {
  url,
  manipulateData,
  hooks,
};
