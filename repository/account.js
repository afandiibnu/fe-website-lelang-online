import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  getAccount: () => `/account`,
  accountEdit: () => `/account/edit/`,
};

const hooks = {
  getAccountDetail() {
    return useSWR(url.getAccount(), http.fetcher);
  },
};

const manipulateData = {
  updateAccount(data) {
    return http.put(url.accountEdit()).send(data);
  },
};

export const accountRepository = {
  url,
  hooks,
  manipulateData,
};
