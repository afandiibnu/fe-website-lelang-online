import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  product: ({ page, limit }) => `/product/all/l?page=${page}&limit=${limit}`,
  productApprove: ({ page, limit, search }) =>
    `/product/all/approve?page=${page}&limit=${limit}&search=${search}`,
  productDigital: ({ page, limitDigital }) =>
    `/product/category/?page=${page}&limit=${limitDigital}&category=digital`,

  productFine: ({ page, limitFine }) =>
    `/product/category/?page=${page}&limit=${limitFine}&category=fine`,

  productDetail: (id) => `/product/get/${id}`,
  searchProduct: ({ limit, page, search }) =>
    `/product/search/product?limit=${limit}&page=${page}&search=${search}`,
  createProduct: () => `/product/create`,
  uploadProductImage: () => `/product/picture`,
  uploadProductImageCopyright: () => `/product/copyright/coba`,
  getMyAuction: () => `/product/get/product/a`,
  getMyAuctionAdmin: (id) => `/product/get/product/${id}`,
  approveProduct: (id) => `/product/approve/${id}/status`,
  rejectProduct: (id) => `/product/rejected/${id}/status`,
  getFineArt: () => `/product/get/fine/a`,
  getDigitalArt: () => `/product/get/digital/b`,
  getBidHistory: ({ page, limit }) =>
    `/logbid/get/status/apakek?page=${page}&limit=${limit}`,
};

const hooks = {
  getAllProduct(filter) {
    return useSWR(url.product(filter), http.fetcher);
  },
  getAllProductApprove(filter) {
    return useSWR(url.productApprove(filter), http.fetcher);
  },
  getProductDigital(filter) {
    return useSWR(url.productDigital(filter), http.fetcher);
  },
  getProductFine(filter) {
    return useSWR(url.productFine(filter), http.fetcher);
  },
  getDetail(id) {
    return useSWR(url.productDetail(id), http.fetcher);
  },
  getMyAuction() {
    return useSWR(url.getMyAuction(), http.fetcher);
  },
  getMyAuctionAdmin(id) {
    return useSWR(url.getMyAuctionAdmin(id), http.fetcher);
  },
  getFineArt() {
    return useSWR(url.getFineArt(), http.fetcher);
  },
  getDigitalArt() {
    return useSWR(url.getDigitalArt(), http.fetcher);
  },
  getSearch(filter) {
    return useSWR(url.searchProduct(filter), http.fetcher);
  },
  getBidHistory(filter) {
    return useSWR(url.getBidHistory(filter), http.fetcher);
  },
};

const manipulateData = {
  createProduct(data) {
    return http.post(url.createProduct()).send(data);
  },
  uploadProductImage(file) {
    console.log(file, "apa isi datanya");
    const formData = new FormData();
    formData.append("picture", file);
    return http.upload(url.uploadProductImage()).send(formData);
  },
  uploadProductImageCopyright(file) {
    console.log(file, "apa isi datanya copyright");
    const formData = new FormData();
    formData.append("copyright", file);
    return http.upload(url.uploadProductImageCopyright()).send(formData);
  },
  approveProduct(id, data) {
    return http.put(url.approveProduct(id)).send(data);
  },
  rejectProduct(id, data) {
    return http.put(url.rejectProduct(id)).send(data);
  },
};

export const productRepository = {
  url,
  manipulateData,
  hooks,
};
