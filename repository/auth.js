import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  register: () => `/auth/register`,
  login: () => `/auth/login`,
};

const hooks = {};

const manipulateData = {
  createUser(data) {
    return http.post(url.register()).send(data);
  },
  userLogin(data) {
    return http.post(url.login()).send(data);
  },
};

export const authRepository = {
  url,
  manipulateData,
  hooks,
};
