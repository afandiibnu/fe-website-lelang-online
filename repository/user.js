import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  user: ({ page, limit, search }) =>
    `/users/search/user?page=${page}&limit=${limit}&search=${search}`,
  userDetail: () => `/users/detail/user/a`,
  userEdit: () => `/users/edit/p`,
  userPictureEdit: () => `/users/edit/picture/a`,
  uploadUserImage: () => `/users/post/picture`,
  uploadKTP: () => `/users/post/ktp`,
  ktpEdit: () => `/users/edit/k`,
  auctioner: () => `/users/get/auctioner`,
  bidder: () => `/users/get/bidder`,
  deleteUser: (id) => `/users/${id}`,
  getUserId: (id) => `/users/detail/user/${id}`,
};

const hooks = {
  getUserDetail() {
    return useSWR(url.userDetail(), http.fetcher);
  },
  getallUser(filter) {
    return useSWR(url.user(filter), http.fetcher);
  },
  getBidder() {
    return useSWR(url.bidder(), http.fetcher);
  },
  getAuctioner() {
    return useSWR(url.auctioner(), http.fetcher);
  },
  getDetail(filter) {
    return useSWR(url.getUserId(filter), http.fetcher);
  },
};

const manipulateData = {
  updateUser(data) {
    return http.put(url.userEdit()).send(data);
  },
  uploadUserImage(file) {
    // console.log(file, "apa isi dtanya");
    const formData = new FormData();
    formData.append("picture", file);
    return http.upload(url.uploadUserImage()).send(formData);
  },
  uploadKTP(file) {
    // console.log(file, "apa isi dtanya");
    const formData = new FormData();
    formData.append("ktp", file);
    return http.upload(url.uploadKTP()).send(formData);
  },
  updateKTP(data) {
    return http.put(url.ktpEdit()).send(data);
  },
  updatePicture(data) {
    return http.put(url.userPictureEdit()).send(data);
  },
  deleteUser(data, id) {
    return http.put(url.deleteUser(id)).send(data);
  },
};

export const userRepository = {
  url,
  hooks,
  manipulateData,
};
