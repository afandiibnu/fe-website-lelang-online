import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  getAllInsurance: ({ page, limit }) =>
    `/insurance-money/get/all/p?page=${page}&limit=${limit}`,
  getAllPayment: ({ page, limit }) =>
    `/payments/get/all/p?page=${page}&limit=${limit}`,
  uploadInsurance: () => `/insurance-money/upload/insurance`,
  uploadPayment: () => `/payments/picture`,
  getInsurance: () => `/insurance-money/get`,
  getInsuranceByProduct: (id) => `/insurance-money/get/${id}`,
  getInsuranceById: (id) => `/insurance-money/get-insurance-by-id/${id}`,
  crateInsurance: (id) => `/insurance-money/createinsurance/${id}`,
  buyItNowPayment: (id) => `/payments/create/${id}`,
  getAllPaymentList: () => `/payments/get/bylogin`,
  updatePayment: (id) => `/payments/update/${id}`,
  getDetailPayment: (id) => `/payments/detail/payment/${id}`,
  approveInsurancePayment: (id) =>
    `/insurance-money/approve/${id}/status/insurance`,
  rejectInsurancePayment: (id) =>
    `/insurance-money/rejected/${id}/status/insurance`,
  approvePayment: (id) => `/payments/approve/${id}/status/payment`,
  RejectPayment: (id) => `/payments/rejected/${id}/status/payment`,
};

const hooks = {
  getInsuranceData(id) {
    return useSWR(url.getInsurance(id), http.fetcher);
  },
  getInsuranceByProductData(id) {
    return useSWR(url.getInsuranceByProduct(id), http.fetcher);
  },
  getInsuranceById(id) {
    return useSWR(url.getInsuranceById(id), http.fetcher);
  },
  getAllInsurance(filter) {
    return useSWR(url.getAllInsurance(filter), http.fetcher);
  },
  getAllPayments(filter) {
    return useSWR(url.getAllPayment(filter), http.fetcher);
  },
  getPaymentList() {
    return useSWR(url.getAllPaymentList(), http.fetcher);
  },
  getPaymentDetail(id) {
    return useSWR(url.getDetailPayment(id), http.fetcher);
  },
};

const manipulateData = {
  uploadInsurancePicture(file) {
    const formData = new FormData();
    formData.append("proof_of_payment", file);
    console.log(file, "ini data");
    return http.upload(url.uploadInsurance()).send(formData);
  },
  uploadPaymentPicture(file) {
    const formData = new FormData();
    formData.append("proof_of_payment", file);
    console.log(file, "ini data");
    return http.upload(url.uploadPayment()).send(formData);
  },
  createInsurance(id, data) {
    return http.post(url.crateInsurance(id)).send(data);
  },
  createBuyItNowPayment(id, data) {
    return http.post(url.buyItNowPayment(id)).send(data);
  },
  updatePayment(id, data) {
    return http.put(url.updatePayment(id)).send(data);
  },
  approveInsurancePayment(id, data) {
    return http.put(url.approveInsurancePayment(id)).send(data);
  },
  rejectedInsurancePayment(id, data) {
    return http.put(url.rejectInsurancePayment(id)).send(data);
  },
  RejectPayment(id, data) {
    return http.put(url.RejectPayment(id)).send(data);
  },
  approvePayment(id, data) {
    return http.put(url.approvePayment(id)).send(data);
  },
};

export const paymentRepository = {
  url,
  manipulateData,
  hooks,
};
