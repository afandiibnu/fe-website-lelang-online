import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDashboard,
  faUser,
  faTag,
  faGavel,
  faMoneyBill,
  faHandHoldingDollar,
} from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";

const Sidebar = () => {
  const router = useRouter();
  const { id } = router.query;
  const currentRoute = router.pathname;

  return (
    <div className="min-h-screen w-1/6 bg-dark-blue box-content">
      <div className="p-10">
        <Image src={"/logoDark.svg"} width={"58px"} height={"32px"} />
      </div>
      <div className="flex flex-col ">
        <div
          className={`${
            currentRoute === "/dashboard"
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard" passHref>
            <a
              className={
                currentRoute === "/dashboard"
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faDashboard} className={"mr-4"} />
              <span>Dashboard</span>
            </a>
          </Link>
        </div>

        <div
          className={`${
            currentRoute === "/dashboard/user" ||
            currentRoute === "/dashboard/user/[id]"
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard/user" passHref>
            <a
              className={
                currentRoute === "/dashboard/user" ||
                currentRoute === "/dashboard/user/[id]"
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faUser} className={"mr-4"} />
              <span className="">User</span>
            </a>
          </Link>
        </div>
        <div
          className={`${
            currentRoute === "/dashboard/product" ||
            currentRoute === "/dashboard/product/[id]"
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard/product" passHref>
            <a
              className={
                currentRoute === "/dashboard/product" ||
                currentRoute === "/dashboard/product/[id]"
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faTag} className={"mr-4"} />
              <span className="">Product</span>
            </a>
          </Link>
        </div>
        <div
          className={`${
            currentRoute === "/dashboard/auction" ||
            currentRoute === "/dashboard/auction/[id]"
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard/auction" passHref>
            <a
              className={
                currentRoute === "/dashboard/auction" ||
                currentRoute === "/dashboard/auction/[id]"
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faGavel} className={"mr-4"} />
              <span className="">Auction</span>
            </a>
          </Link>
        </div>
        <div
          className={`${
            currentRoute === "/dashboard/payment" ||
            currentRoute === "/dashboard/payment/[id]"
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard/payment" passHref>
            <a
              className={
                currentRoute === "/dashboard/payment" ||
                currentRoute === "/dashboard/payment/[id]"
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faMoneyBill} className={"mr-4"} />
              <span className="">Payment</span>
            </a>
          </Link>
        </div>
        <div
          className={`${
            currentRoute === "/dashboard/insurance" ||
            currentRoute === `/dashboard/insurance/[id]`
              ? "px-10 py-6 text-sm font-medium cursor-pointer bg-dark-blue-secondary border-l-4 border-primary"
              : "px-10 py-6 text-sm font-medium cursor-pointer"
          }`}
        >
          <Link href="/dashboard/insurance" passHref>
            <a
              className={
                currentRoute === "/dashboard/insurance" ||
                currentRoute === `/dashboard/insurance/[id]`
                  ? "text-white"
                  : "text-color-secondary"
              }
            >
              <FontAwesomeIcon icon={faHandHoldingDollar} className={"mr-4"} />
              <span className="">insurance money</span>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
