import { SearchOutlined } from "@ant-design/icons";

const SearchAdmin = (props) => {
  return (
    <div className="relative">
      <label className="">
        <span className="absolute inset-y-0 left-4 flex items-center text-color-secondary text-md">
          <SearchOutlined></SearchOutlined>
        </span>
        <input
          className={`placeholder:text-color-secondary h-12 block rounded py-2 pl-10 pr-3 focus:outline-none text-sm ${props.color} w-full focus:ring-2 focus:shadow-md focus:shadow-primary/20 transition duration-100 bg-secondary`}
          placeholder="Search....."
          type="text"
          name="search"
          autoComplete="off"
        />
      </label>
    </div>
  );
};

export default SearchAdmin;
