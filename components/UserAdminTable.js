import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useMemo } from "react";
import { useTable } from "react-table";
import mutate from "swr";
import { productRepository } from "../repository/product";

import SearchAdmin from "./SearchAdmin";

const AdminUserTable = (props) => {
  const { API = [], cols = [] } = props;
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(2);

  const columns = useMemo(() => cols, []);
  const data = useMemo(() => API, []);
  console.log(data, "daataa tapi di komponen");
  const tableInstance = useTable({ columns, data });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    tableInstance;

  const rejectProduct = async (id) => {
    try {
      const data = { status: "rejected" };
      await productRepository.manipulateData.rejectProduct(id, data);
      console.log("berhasil");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="bg-white rounded-lg p-10">
      <div className="pb-6">
        <SearchAdmin />
      </div>

      <div>
        <div className="bg-secondary w-full h-[1px]"></div>
        <table {...getTableProps()} className="w-full text-sm table-auto ">
          <thead className="">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()} className="bg-white">
                <td></td>
                {headerGroup.headers.map((column) => (
                  <td
                    {...column.getHeaderProps()}
                    className="font-medium px-4 py-3"
                  >
                    {column.render("Header")}
                  </td>
                ))}
                <td className="font-medium w-10">Actions</td>
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  <td>
                    <div className="border rounded-full w-[20px] h-[20px] overflow-hidden cursor-pointer">
                      <Link href={`/dashboard/product/${row.original.id}`}>
                        <img
                          src={`http://localhost:3222/users/get-profile-image/${row.original.picture}`}
                        />
                      </Link>
                    </div>
                  </td>

                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="px-4 py-3">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}

                  <td className="px-4 py-3">
                    <FontAwesomeIcon
                      icon={faTrash}
                      className="text-red-500 ml-5 cursor-pointer"
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="flex justify-center gap-x-3">
          <div>
            <button
              onClick={() => {
                setPage(page - 1);
              }}
            >
              kiri
            </button>
          </div>
          <button
            onClick={() => {
              setPage(page + 1);
            }}
          >
            kanan
          </button>
        </div>
      </div>
    </div>
  );
};

export default AdminUserTable;
