import { SearchOutlined } from "@ant-design/icons";
import { useState } from "react";
import { productRepository } from "../repository/product";

const Search = (props) => {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");
  const searchHandler = async (e) => {
    e.preventDefault();

    try {
      setSearch(e.target.value);
      console.log(search);

      await productRepository.hooks.getSearch({ page, limit, search });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="relative">
      <label className="">
        <span className="sr-only">Search</span>
        <span className="absolute inset-y-0 left-4 flex items-center text-color-secondary text-md">
          <SearchOutlined></SearchOutlined>
        </span>
        <form method="post">
          <input
            className={`placeholder:text-color-secondary h-10 block rounded-full py-2 pl-10 pr-3 focus:outline-none text-sm ${props.color} w-full focus:ring-2 focus:shadow-md focus:shadow-primary/20 transition duration-100`}
            placeholder="Search for auction..."
            type="text"
            name="search"
            autoComplete="off"
            onChange={searchHandler}
          />
        </form>
      </label>
    </div>
  );
};

export default Search;
