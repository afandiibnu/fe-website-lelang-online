import React from "react";
import ButtonPrimary from "./ButtonPrimary";
import ButtonSecondary from "./ButtonSecondary";

const ButtonGroup = () => {
  return (
    <div className="flex w-60 items-center space-x-2">
      <ButtonSecondary href="/login" text="Sign In" padding="py-2.5 px-8" />
      <ButtonPrimary href="/register" text="Sign Up" padding="py-2.5 px-8" />
    </div>
  );
};

export default ButtonGroup;
