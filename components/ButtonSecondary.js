import Link from "next/link";

const ButtonSecondary = (props) => {
  return (
    <>
      <Link href={props.href}>
        <button
          className={`text-sm text-center bg-secondary-purple ${props.padding} ${props.width} rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150`}
        >
          {props.text}
        </button>
      </Link>
    </>
  );
};

export default ButtonSecondary;
