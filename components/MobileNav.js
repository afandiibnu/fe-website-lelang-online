import Image from "next/image";
import ButtonPrimary from "./ButtonPrimary";
import ButtonSecondary from "./ButtonSecondary";
import Navlink from "./Navlink";
import NavlinkDropdown from "./NavlinkDropdown";
import Search from "./Search";
import { useState } from "react";

const MobileNav = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="lg:hidden bg-white relative">
      <div className="flex w-full justify-between px-10 py-5  bg-white">
        <Image src={"/logoLight.svg"} width="36px" height="24px" />
        <button onClick={() => setIsOpen(!isOpen)}>
          <svg
            width="36"
            height="36"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M21 16H3V14H21V16ZM21 10H3V8H21V10Z" fill="#171717"></path>
          </svg>
        </button>
      </div>
      <div
        className={`${
          isOpen ? "opacity-100" : "opacity-0"
        } block pt-8 min-h-screen px-10 space-y-4 w-full absolute bg-white transition duration-400`}
      >
        <Navlink to="/" text="Auctions" />
        <NavlinkDropdown />
        <div className="pt-8 space-y-9">
          <Search />
          <div className="space-y-4">
            <ButtonPrimary
              href="/register"
              text="Sign Up"
              width="w-full"
              padding="py-2.5"
            />
            <ButtonSecondary
              href="/login"
              text="Sign In"
              width="w-full"
              padding="py-2.5"
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default MobileNav;
