import ButtonPrimary from "./ButtonPrimary";
const SellSection = () => {
  return (
    <div className="grid place-items-center md:place-items-start grid-cols-1 lg:grid-cols-2 w-full min-h-screen px-10 lg:px-20 xl:px-40 2xl:px-72 gap-x-40 ">
      <div className="md:row-start-1 items-center mt-32">
        <img
          src={"/img2.png"}
          className="w-[275px] lg:w-[560px] h-[334px] lg:h-[680px]"
        />
      </div>
      <div className="flex flex-col justify-center md:row-start-1 md:col-start-1 h-full content-center">
        <h1 className="text-2xl md:text-3xl lg:text-4xl font-bold text-center md:text-left">
          Show your art to the<br></br> world
        </h1>
        <p className="text-color-secondary text-center md:text-start text-base mt-2 md:mt-6">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Purus neque
          vivamus vehicula duis purus
        </p>
        <div className="mt-10 md:mt-12 mb-10">
          <ButtonPrimary
            href="/user/sell"
            text="Sell your art"
            width="w-[120px]"
            padding="py-4"
          />
        </div>
      </div>
    </div>
  );
};

export default SellSection;
