import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useMemo } from "react";
import { useTable } from "react-table";
import { mutate } from "swr";
import { productRepository } from "../repository/product";

import SearchAdmin from "./SearchAdmin";

const AdminTable = (props) => {
  const { API = [], cols = [] } = props;

  const columns = useMemo(() => cols, []);
  const data = useMemo(() => API, []);

  const tableInstance = useTable({ columns, data });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    tableInstance;

  const updateProduct = async (id) => {
    try {
      const data = { status: "approve" };
      await productRepository.manipulateData.approveProduct(id, data);
      await mutate(productRepository.url.product({ page, limit }));
      console.log("berhasil");
    } catch (e) {
      console.log(e.response);
    }
  };

  const rejectProduct = async (id) => {
    try {
      const data = { status: "rejected" };
      await productRepository.manipulateData.rejectProduct(id, data);
      await mutate(productRepository.url.product({ page, limit }));
      console.log("berhasil");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="bg-white p-10">
      <div className="pb-3  ">
        <SearchAdmin />
      </div>

      <div>
        <div className="bg-secondary w-full h-[1px]"></div>
        <table {...getTableProps()} className="w-full text-sm table-auto ">
          <thead className="">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()} className="bg-white">
                <td></td>
                {headerGroup.headers.map((column) => (
                  <td
                    {...column.getHeaderProps()}
                    className="font-medium px-4 py-3"
                  >
                    {column.render("Header")}
                  </td>
                ))}
                <td className="font-medium w-10">Actions</td>
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  <td>
                    {" "}
                    <div className="border rounded-full w-[20px] h-[20px] overflow-hidden ">
                      <img
                        src={`http://localhost:3222/product/get-product-image/${row.original.picture}`}
                      />
                    </div>
                  </td>

                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="px-4 py-3">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}

                  <td className="px-4 py-3">
                    <div className="flex gap-x-4">
                      <button
                        className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                        type="submit"
                        onClick={() => updateProduct(row.original.id)}
                      >
                        Approve
                      </button>
                      <div>
                        <Link href={`/dashboard/product/${row.original.id}`}>
                          <button
                            className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                            type="submit"
                          >
                            detail
                          </button>
                        </Link>
                      </div>
                      <div>
                        <button
                          className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                          type="submit"
                          onClick={() => rejectProduct(row.original.id)}
                        >
                          Rejected
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default AdminTable;
