import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useMemo } from "react";
import { useTable } from "react-table";
import mutate from "swr";
import { productRepository } from "../repository/product";

import SearchAdmin from "./SearchAdmin";

const InsurancePaymentTable = (props) => {
  const { API = [], cols = [] } = props;

  const columns = useMemo(() => cols, []);
  const data = useMemo(() => API, []);

  const tableInstance = useTable({ columns, data });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    tableInstance;

  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);

  const { data: product } = productRepository.hooks.getAllProduct({
    page,
    limit,
  });

  const updateProduct = async (id) => {
    try {
      const data = { status: "approve" };
      await productRepository.manipulateData.approveProduct(id, data);
      await mutate(productRepository.url.product({ page, limit }));
      console.log("berhasil");
    } catch (e) {
      console.log(e.response);
    }
  };
  useEffect(() => {
    if (product !== null && product !== undefined) {
      setProducts(product.items);
    }
  }, [product]);

  const rejectProduct = async (id) => {
    try {
      const data = { status: "rejected" };
      await productRepository.manipulateData.rejectProduct(id, data);
      await mutate(productRepository.url.product({ page, limit }));
      console.log("berhasil");
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="bg-white px-10">
      <div className=" pt-6 pb-3  ">
        <SearchAdmin />
      </div>

      <div>
        <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
        <table {...getTableProps()} className="w-full text-sm table-auto ">
          <thead className="">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()} className="bg-white">
                {headerGroup.headers.map((column) => (
                  <td
                    {...column.getHeaderProps()}
                    className="font-medium px-4 py-3"
                  >
                    {column.render("Header")}
                  </td>
                ))}
                <td className="font-medium w-10">Actions</td>
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="px-4 py-3">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}

                  <td className="px-4 py-3">
                    <div className="flex gap-x-4">
                      <button
                        className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                        type="submit"
                        onClick={() => updateProduct(row.original.id)}
                      >
                        Approve
                      </button>
                      <div>
                        <Link href={`/dashboard/insurance/${row.original.id}`}>
                          <button
                            className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                            type="submit"
                          >
                            detail
                          </button>
                        </Link>
                      </div>
                      <div>
                        <button
                          className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                          type="submit"
                          onClick={() => rejectProduct(row.original.id)}
                        >
                          Rejected
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default InsurancePaymentTable;
