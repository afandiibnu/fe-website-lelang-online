import Image from "next/image";
import React from "react";

const Banner = ({ children, ...props }) => {
  return (
    <div
      className={`w-full ${props.height} bg-dark-blue bg-[url('/pattern.png')]`}
    >
      {children}
    </div>
  );
};

export default Banner;
