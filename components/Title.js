const Title = (props) => {
  return (
    <>
      <h1 className={`${props.size} text-${props.color} font-bold`}>
        {props.title}
      </h1>
    </>
  );
};

export default Title;
