const FooterList = ({ children }) => {
  return (
    <div className="flex flex-col gap-y-4 text-color-secondary mt-6">
      {children}
    </div>
  );
};

export default FooterList;
