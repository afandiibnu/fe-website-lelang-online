import Link from "next/link";
import FooterList from "./FooterList";
import Title from "./Title";
import Image from "next/image";

const Footer = () => {
  return (
    <div className="bg-dark-blue px-40 ">
      <div className="py-28 flex flex-col md:flex-row justify-center text-white gap-x-16">
        <div>
          <Title title="AUCTIONS" size="text-sm" />
          <FooterList>
            <Link href={"/digital"}>
              <a>Digital arts</a>
            </Link>
            <Link href={"/fine"}>
              <a>Fine arts</a>
            </Link>
            <Link href={"/ongoing"}>
              <a>Ongoing auctions</a>
            </Link>
            <Link href={"/finished"}>
              <a>Finished auctions</a>
            </Link>
          </FooterList>
        </div>
        <div>
          <Title title="COMPANY" size="text-sm" />
          <FooterList>
            <Link href={"/about"}>
              <a>About</a>
            </Link>
            <Link href={"/terms-condition"}>
              <a>Terms and condition</a>
            </Link>
          </FooterList>
        </div>
        <div>
          <Title title="HELP" size="text-sm" />
          <FooterList>
            <Link href={"/how-to-buy"}>
              <a>How to buy art</a>
            </Link>
            <Link href={"/hot-to-sell"}>
              <a>How to sell </a>
            </Link>
            <Link href={"/faq"}>
              <a>FAQ</a>
            </Link>
          </FooterList>
        </div>
        <div>
          <Title title="SOCIAL MEDIA" size="text-sm" />
          <FooterList>
            <Link href={"/Facebook"}>
              <a>Facebook</a>
            </Link>
            <Link href={"/Instagram"}>
              <a>Instagram</a>
            </Link>
            <Link href={"/Twitter"}>
              <a>Twitter</a>
            </Link>
          </FooterList>
        </div>
      </div>
      <hr className="border-color-secondary"></hr>
      <div className="py-10 flex justify-center w-full">
        <Image src={"/logoDark.svg"} width={"76px"} height={"40px"} />
      </div>
    </div>
  );
};

export default Footer;
