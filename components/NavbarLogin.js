import Image from "next/image";
import Search from "./Search";
import ButtonPrimary from "./ButtonPrimary";
import ButtonSecondary from "./ButtonSecondary";
import NavlinkDropdown from "./NavlinkDropdown";
import Navlink from "./Navlink";
import MobileNav from "./MobileNav";
import UserDropdown from "./UserDropdown";
import ButtonGroup from "./ButtonGroup";
import Link from "next/link";

const NavbarLogin = (props) => {
  return (
    <div className="">
      <MobileNav />
      <div
        className={`w-full py-5 px-20 hidden lg:flex items-center relative z-10 ${props.color} ${props.textColor}`}
      >
        <Link href={"/"}>
          <Image
            src={`/logo${props.logoColor}.svg`}
            width="58"
            height="32"
            layout="intrinsic"
          />
        </Link>

        <div className="flex justify-between items-center w-full ml-4 xl:ml-16 ">
          <div className="flex items-center space-x-14 ">
            <div className="flex text-sm items-center gap-x-4">
              <Navlink to="/" text="Auctions" />
              <NavlinkDropdown color={props.color} />
              <Navlink to="/search" text="Search auction" />
            </div>
          </div>
          <UserDropdown />
        </div>
      </div>
    </div>
  );
};

export default NavbarLogin;
