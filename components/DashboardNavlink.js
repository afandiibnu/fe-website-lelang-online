import Link from "next/link";
import { useState } from "react";

const DashboardNavlink = ({ children, ...props }) => {
  return (
    <Link href={`${props.to}`}>
      <div className="flex items-center px-10 py-6 bg-dark-blue-secondary border-l-4 border-primary font-bold text-sm cursor-pointer">
        {children}
      </div>
    </Link>
  );
};

export default DashboardNavlink;
