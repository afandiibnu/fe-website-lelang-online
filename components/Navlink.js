import Link from "next/link";

const Navlink = (props) => {
  return (
    <>
      <Link href={props.to}>
        <a className={`font-medium ${props.color} text-sm`}>{props.text}</a>
      </Link>
    </>
  );
};

export default Navlink;
