import React from "react";

const Countdown = (props) => {
  return (
    <div className="flex text-xs gap-x-2 text-light-green font-bold">
      <div className="bg-black/60 p-2 text-center">
        <span>{props.days} D</span>
      </div>
      <div className="bg-black/60 p-2  text-center">
        <span>{props.hours} H</span>
      </div>
      <div className="bg-black/60 p-2 text-center">
        <span>{props.minutes} M</span>
      </div>
    </div>
  );
};

export default Countdown;
