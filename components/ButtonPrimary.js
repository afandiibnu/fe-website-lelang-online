import Link from "next/link";

const ButtonPrimary = (props) => {
  return (
    <div>
      <Link href={props.href}>
        <button
          className={`text-sm text-center bg-primary ${props.padding} ${props.width} rounded-full text-white font-medium  hover:shadow-md hover:shadow-primary/40 transiton duration-150 focus:ring-2`}
        >
          {props.text}
        </button>
      </Link>
    </div>
  );
};

export default ButtonPrimary;
