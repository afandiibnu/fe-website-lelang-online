import ButtonPrimary from "./ButtonPrimary";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { parseJwt } from "../helper/DecodeJwt";
import Typewriter from "typewriter-effect";

const Hero = () => {
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== "undefined") {
      token = localStorage.getItem("token");
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
  }, []);

  return (
    <div className="grid md:place-items-start grid-cols-1 lg:grid-cols-2 w-full bg-dark-blue h-[700px] md:h-[580px] bg-[url('/pattern.png')] px-10 lg:px-20 xl:px-40 2xl:px-72">
      <div className="mt-14 lg:-mt-16 md:row-start-1 lg:place-self-center 2xl:place-self-end">
        <img
          src={"/img2.png"}
          className="w-[275px] lg:w-[560px] h-[334px] lg:h-[680px]"
        />
      </div>
      <div className="flex flex-col justify-center items-center h-full md:row-start-1 md:items-start md:col-start-1">
        <h1 className="text-2xl md:text-3xl lg:text-4xl 2xl:text-6xl text-white font-bold text-center md:text-left leading-rela">
          Discover The
          <span className="text-light-green">
            <Typewriter
              options={{ autoStart: true, loop: true }}
              onInit={(typewriter) => {
                typewriter
                  .typeString("Greatest")
                  .pauseFor(2000)
                  .deleteAll()
                  .typeString("Biggest")
                  .pauseFor(2000)
                  .start();
              }}
            />
          </span>
          Art Auction
        </h1>
        <p className="text-color-secondary text-center md:text-start text-base mt-2 md:mt-6">
          Discover digital art, fine art around the world.
        </p>
        <div className="mt-10 md:mt-12 mb-10">
          {isLogin ? (
            ""
          ) : (
            <ButtonPrimary
              href="/register"
              text="Get Started"
              width="w-[120px]"
              padding="py-4"
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default Hero;
