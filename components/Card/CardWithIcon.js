import CardIcon from "./CardIcon";
import CardTitle from "./CardTitle";
import CardSubtitle from "./CardSubtitle";

const CardWithIcon = (props) => {
  return (
    <div
      className={`bg-${
        props.background ? props.background : "white"
      } rounded-md h-[250px] 2xl:h-[300px] drop-shadow-2xl p-9 2xl:p-16 relative`}
    >
      <div className="space-y-6">
        <CardIcon icon={props.icon} />
        <div className="space-y-4">
          <CardTitle
            title={props.title}
            color={props.titleColor}
            weight="medium"
          />
          <CardSubtitle subtitle={props.subtitle} color={props.subtitleColor} />
        </div>
      </div>
    </div>
  );
};

export default CardWithIcon;
