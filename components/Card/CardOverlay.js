import Link from "next/link";

const CardOverlay = ({ children, ...props }) => {
  return (
    <Link href={props.href}>
      <div className="h-[480px] bg-secondary rounded-md relative overflow-hidden cursor-pointer">
        {children}
        <div className="bg-gradient-to-b from-transparent to-black/50 w-full min-h-full absolute"></div>
      </div>
    </Link>
  );
};

export default CardOverlay;
