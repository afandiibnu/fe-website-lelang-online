import ButtonOutline from "../ButtonOutline";
import Countdown from "../Countdown";
import CardSubtitle from "./CardSubtitle";
import CardTitle from "./CardTitle";

const CardWithButton = (props) => {
  return (
    <div>
      <div className="mb-8">
        <div className="h-52 overflow-hidden rounded-lg relative mb-4">
          <div className=" absolute bottom-4 right-4">
            <Countdown
              days={props.days}
              hours={props.hours}
              minutes={props.minutes}
            />
          </div>
          <img src={props.image} className="w-full transition duration-300" />
        </div>
        <CardTitle
          size={props.size}
          title={props.title}
          color={props.titleColor}
          weight={props.weight}
        />
        <CardSubtitle subtitle={props.subtitle} color={props.subtitleColor} />
      </div>

      <ButtonOutline href={props.href} text={props.text} padding="p-2" />
    </div>
  );
};

export default CardWithButton;
