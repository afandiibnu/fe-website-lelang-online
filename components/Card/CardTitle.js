import React from "react";

const CardTitle = (props) => {
  return (
    <h1
      className={`text-${props.size ? props.size : "xl"} font-${
        props.weight ? props.weight : "bold"
      } text-${props.color} mb-2`}
    >
      {props.title}
    </h1>
  );
};

export default CardTitle;
