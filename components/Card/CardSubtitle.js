import React from "react";

const CardSubtitle = (props) => {
  return (
    <p
      className={`text-sm text-${props.color ? props.color : "white"} font-${
        props.weight
      }`}
    >
      {props.subtitle}
    </p>
  );
};

export default CardSubtitle;
