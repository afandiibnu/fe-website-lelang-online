import Image from "next/image";

const CardIcon = ({ children, ...props }) => {
  return (
    <div
      className={`rounded-full flex justify-center items-center bg-${
        props.background ? props.background : "secondary-purple"
      } w-12 2xl:w-14 h-12 2xl:h-14`}
    >
      <img src={props.icon} className="w-[24px] h-[24px]" />
    </div>
  );
};

export default CardIcon;
