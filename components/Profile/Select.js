const Input = ({ ...props }) => {
  return (
    <div className="mb-6">
      <label for={props.name} className="text-sm font-medium">
        {props.label}
      </label>
      <input
        type={props.type}
        name={props.name}
        id={props.name}
        value={props.value}
        className="w-full outline-none border border-gray-300 h-10 px-4 rounded focus:ring-1 ring-primary/30 mt-2 text-sm"
      />
    </div>
  );
};

export default Input;
