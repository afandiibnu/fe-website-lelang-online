import React from "react";

const Input = ({ ...props }) => {
  return (
    <div className="mb-6">
      <label htmlFor={props.name} className="text-sm font-medium">
        {props.label}
      </label>
      <input
        type={props.type}
        name={props.name}
        id={props.name}
        defaultValue={props.value}
        placeholder={props.placeholder}
        onChange={props.onChange}
        autoComplete="off"
        className="w-full outline-none border border-gray-300 h-10 px-4 rounded focus:ring-1 ring-primary/30 mt-2 text-sm placeholder:capitalize"
      />
    </div>
  );
};

export default Input;
