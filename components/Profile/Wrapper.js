import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const Wrapper = ({ children, ...props }) => {
  const router = useRouter();
  const pathname = router.pathname;

  const isInsurance =
    router.pathname == "/user/payment/payment-receipt-insurance/[id]";

  return (
    <div className="border border-[#e8e8e8] rounded-md py-4 divide-y">
      <div className="mb-6 px-6">
        <div className="flex items-center gap-x-2">
          <h1 className="text-lg font-bold text-primary">{props.title}</h1>
          {isInsurance && (
            <Link href="/terms-condition">
              <span className="bg-secondary px-2 rounded-full text-primary font-bold cursor-pointer">
                ?
              </span>
            </Link>
          )}
        </div>
        <p className="text-color-secondary text-sm mt-2">{props.subtitle}</p>
      </div>
      <div className={`${props.padding}`}>{children}</div>
    </div>
  );
};

export default Wrapper;
