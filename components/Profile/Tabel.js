import React from "react";
import ButtonPrimary from "../ButtonPrimary";
import { useMemo } from "react";
import { useTable } from "react-table";

const Tabel = (props) => {
  const { API = [], cols = [] } = props;

  const columns = useMemo(() => cols, []);
  const data = useMemo(() => API, []);

  const tableInstance = useTable({ columns, data });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    tableInstance;
  let i = 1;

  return (
    <table {...getTableProps()} className="w-full text-sm table-auto">
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr
            {...headerGroup.getHeaderGroupProps()}
            className="bg-secondary-purple"
          >
            <td className="font-medium px-6 py-3 w-1">#</td>
            {headerGroup.headers.map((column) => (
              <td
                {...column.getHeaderProps()}
                className="font-medium px-6 py-3"
              >
                {column.render("Header")}
              </td>
            ))}
            <td className="font-medium">Actions</td>
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              <td className="px-6 py-3">{i++}</td>
              {row.cells.map((cell) => {
                return (
                  <td {...cell.getCellProps()} className="px-6 py-3">
                    {cell.render("Cell")}
                  </td>
                );
              })}
              <td>
                <ButtonPrimary
                  href={`/user/my-auctions/${row.original.id}`}
                  text="Detail"
                  padding="px-3 py-1"
                />
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Tabel;
