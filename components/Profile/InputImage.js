import React from "react";

const InputImage = ({ ...props }) => {
  return (
    <div className="flex items-center space-x-6 mt-2">
      <div className="shrink-0">
        <img
          className="h-16 w-16 object-cover rounded-full"
          src="https://images.unsplash.com/photo-1580489944761-15a19d654956?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1361&q=80"
          alt="Current profile photo"
        />
      </div>
      <label className="block">
        <span className="sr-only"></span>
        <input
          type="file"
          className="block w-full text-sm text-color-secondary file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-reguler file:text-primary file:bg-secondary-purple"
          name="picture"
        />
      </label>
    </div>
  );
};

export default InputImage;
