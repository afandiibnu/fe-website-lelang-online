import React, { useEffect, useState } from "react";
import Link from "next/link";
import { SettingFilled } from "@ant-design/icons";
import {
  faUser,
  faGavel,
  faClockRotateLeft,
  faMoneyBill,
  faRepeat,
  faRightFromBracket,
  faPlusSquare,
  faMoneyBillTransfer,
  faMoneyCheckDollar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { mutate } from "swr";
import { parseJwt } from "../../helper/DecodeJwt";

const Sidebar = () => {
  const router = useRouter();
  const { id } = router.query;
  const currentRoute = router.pathname;
  const [isAuctioner, setIsAuctioner] = useState(false);

  const logoutHandler = async () => {
    localStorage.removeItem("token");

    mutate(await router.push("/"));

    router.reload();
  };

  let token;

  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  useEffect(() => {
    const decodeJwt = parseJwt(token);
    if (decodeJwt?.result?.account?.role?.description == "Auctioner") {
      setIsAuctioner(true);
    }
  });

  mutate(isAuctioner);

  return (
    <div>
      {isAuctioner ? (
        <div className="space-y-6 text-sm font-medium">
          <ul className="cursor-pointer">
            <Link href="/user">
              <li
                className={
                  currentRoute === "/user"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faUser} />
                <a className="ml-4">Profile</a>
              </li>
            </Link>
            <Link href="/user/account">
              <li
                className={
                  currentRoute === "/user/account"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <SettingFilled></SettingFilled>
                <a className="ml-4">Account</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/bid-history/">
              <li
                className={
                  currentRoute === "/user/bid-history"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faClockRotateLeft} />
                <a className="ml-4">Bid history</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/my-auctions/">
              <li
                className={
                  currentRoute === "/user/my-auctions" ||
                  currentRoute === `/user/my-auctions/[id]`
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faGavel} />
                <a className="ml-4">My auctions</a>
              </li>
            </Link>
            <Link href="/user/sell">
              <li
                className={
                  currentRoute === "/user/sell"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faPlusSquare} />
                <a className="ml-4">Sell art</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/payment/payment-list/">
              <li
                className={
                  currentRoute === "/user/payment/payment-list" ||
                  currentRoute === "/user/payment/payment-detail/[id]"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faMoneyBill} />
                <a className="ml-4">Payments</a>
              </li>
            </Link>

            <Link href="/user/payment/payment-insurance">
              <li
                className={
                  currentRoute === "/user/payment/payment-insurance" ||
                  currentRoute === "/user/payment/payment-insurance/[id]" ||
                  currentRoute ===
                    "/user/payment/payment-receipt-insurance/[id]"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faMoneyBillTransfer} />
                <a className="ml-4">Insurance money</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <button onClick={logoutHandler}>
              <li className="mb-4 text-color-secondary">
                <FontAwesomeIcon icon={faRightFromBracket} />
                <a className="ml-4">Logout</a>
              </li>
            </button>
          </ul>
        </div>
      ) : (
        <div className="space-y-6 text-sm font-medium">
          <ul className="cursor-pointer">
            <Link href="/user">
              <li
                className={
                  currentRoute === "/user"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faUser} />
                <a className="ml-4">Profile</a>
              </li>
            </Link>
            <Link href="/user/account">
              <li
                className={
                  currentRoute === "/user/account"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <SettingFilled></SettingFilled>
                <a className="ml-4">Account</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/bid-history/">
              <li
                className={
                  currentRoute === "/user/bid-history"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faClockRotateLeft} />
                <a className="ml-4">Bid history</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/payment/payment-list/">
              <li
                className={
                  currentRoute === "/user/payment/payment-list" ||
                  currentRoute === "/user/payment/payment-detail/[id]"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faMoneyBill} />
                <a className="ml-4">Payments</a>
              </li>
            </Link>

            <Link href="/user/payment/payment-insurance">
              <li
                className={
                  currentRoute === "/user/payment/payment-insurance" ||
                  currentRoute === "/user/payment/payment-insurance/[id]" ||
                  currentRoute ===
                    "/user/payment/payment-receipt-insurance/[id]"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faMoneyCheckDollar} />
                <a className="ml-4">Insurance</a>
              </li>
            </Link>
          </ul>
          <div className="bg-gradient-to-r from-[#e8e8e8] to-transparent w-full h-[2px]"></div>
          <ul className="cursor-pointer">
            <Link href="/user/register-auctioner">
              <li
                className={
                  currentRoute === "/user/register-auctioner"
                    ? "mb-4 flex items-center text-primary"
                    : "mb-4 flex items-center text-color-secondary"
                }
              >
                <FontAwesomeIcon icon={faRepeat} />
                <a className="ml-4">Become an auctioner</a>
              </li>
            </Link>
            <button onClick={logoutHandler}>
              <li className="mb-4 text-color-secondary">
                <FontAwesomeIcon icon={faRightFromBracket} />
                <a className="ml-4">Logout</a>
              </li>
            </button>
          </ul>
        </div>
      )}
    </div>
  );
};

export default Sidebar;
