const Label = (props) => {
  return (
    <>
      <label htmlFor={props.target} className="mb-2 text-sm font-medium">
        {props.text}
      </label>
    </>
  );
};

export default Label;
