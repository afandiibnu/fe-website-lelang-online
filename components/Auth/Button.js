const Button = ({ ...props }) => {
  return (
    <button
      className={`${props.width} py-2.5 bg-primary rounded font-medium text-white text-sm hover:bg-primary-hover trasition duration-300 mt-4 focus:ring`}
      type={props.type}
    >
      {props.action}
    </button>
  );
};

export default Button;
