const Input = (props) => {
  return (
    <input
      type={props.type}
      name={props.name}
      id={props.id}
      className="h-10 border bg-white  rounded-md focus:ring-1 active:bg-white  focus:outline-none pl-2.5 transition duration-100"
      onChange={props.onChange}
      autoComplete="off"
    />
  );
};

export default Input;
