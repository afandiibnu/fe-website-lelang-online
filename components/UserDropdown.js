import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ChevronDown } from "akar-icons";
import { useState, useEffect } from "react";
import Link from "next/link";
import { userRepository } from "../repository/user";
import { useRouter } from "next/router";
import { mutate } from "swr";
import { parseJwt } from "../helper/DecodeJwt";

const UserDropdown = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useState(null);
  const router = useRouter();
  const [isAuctioner, setIsAuctioner] = useState(false);

  const { data: userData } = userRepository.hooks.getUserDetail();

  const logoutHandler = async () => {
    localStorage.removeItem("token");
    mutate(router.push("/"));
    router?.reload();
  };

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData.data);
    }
  }, [userData]);

  let token;

  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  useEffect(() => {
    const decodeJwt = parseJwt(token);
    if (decodeJwt?.result?.account?.role?.description == "Auctioner") {
      setIsAuctioner(true);
    }
  });

  const name = user?.name;
  const pp = user?.picture;

  return (
    <div>
      <Menu className="relative mr-0 md:mr-8 z-10" as="div">
        <Menu.Button
          as="div"
          className={`text-sm font-medium ${props.color} cursor-pointer flex items-center justify-between md:justify-start`}
          onClick={() => setIsOpen(!isOpen)}
        >
          <div className="flex items-center gap-x-4">
            <div className="w-8 h-8 overflow-hidden rounded-full">
              <img
                src={`http://localhost:3222/users/get-profile-image/${pp}`}
              ></img>
            </div>

            <p className="capitalize">{name}</p>
          </div>

          <div
            className={`${
              isOpen ? "rotate-180" : "rotate-0"
            } ml-2 transition duration-100 flex items-center gap-x-2`}
          >
            <ChevronDown size={14} strokeWidth={4} />
          </div>
        </Menu.Button>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items
            as="div"
            className="bg-white absolute right-0 mt-4 flex flex-col w-full md:w-56 py-6 px-3 outline-none drop-shadow-md rounded text-sm text-color-secondary "
          >
            <Link href="/user/">
              <Menu.Item
                as={"div"}
                className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm cursor-pointer"
              >
                Profile
              </Menu.Item>
            </Link>
            <Link href="/user/account">
              <Menu.Item
                as={"div"}
                className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm cursor-pointer"
              >
                Account
              </Menu.Item>
            </Link>
            <Link href="/user/bid-history">
              <Menu.Item
                as={"div"}
                className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm cursor-pointer"
              >
                Bid History
              </Menu.Item>
            </Link>
            <Link href="/user/payment/payment-list">
              <Menu.Item
                as={"div"}
                className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm cursor-pointer"
              >
                Payment
              </Menu.Item>
            </Link>
            {isAuctioner ? (
              <Link href="/user/my-auctions">
                <Menu.Item
                  as={"div"}
                  className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm cursor-pointer"
                >
                  My auction
                </Menu.Item>
              </Link>
            ) : (
              ""
            )}

            <Menu.Item
              as={"div"}
              className="w-full hover:bg-secondary rounded-sm cursor-pointer"
            >
              <button
                onClick={logoutHandler}
                className="w-full py-2 text-start px-2"
              >
                Logout
              </button>
            </Menu.Item>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
};

export default UserDropdown;
