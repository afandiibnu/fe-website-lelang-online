import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { ChevronDown } from "akar-icons";
import { useState } from "react";
import Link from "next/link";

const NavlinkDropdown = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Menu className="relative z-10" as="div">
      <Menu.Button
        as="div"
        className={`text-sm font-medium ${props.color} cursor-pointer flex items-center justify-between md:justify-start`}
        onClick={() => setIsOpen(!isOpen)}
      >
        <p>Category</p>
        <div
          className={`${
            isOpen ? "rotate-180" : "rotate-0"
          } ml-2 transition duration-100`}
        >
          <ChevronDown size={14} strokeWidth={4} />
        </div>
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items
          as="div"
          className="bg-white absolute left-0 mt-4 flex flex-col w-full md:w-56 py-6 px-3 outline-none drop-shadow-md rounded-md text-sm font-medium text-color-secondary"
        >
          <Link href="/product/categories/digital">
            <Menu.Item
              as={"div"}
              className="w-full hover:bg-secondary py-2 pl-2 mb-2 rounded-sm"
            >
              <a>Digital Art</a>
            </Menu.Item>
          </Link>
          <Link href="/product/categories/fine">
            <Menu.Item
              as={"div"}
              className="w-full hover:bg-secondary py-2 pl-2 rounded-sm"
            >
              <a>Fine Art</a>
            </Menu.Item>
          </Link>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default NavlinkDropdown;
