import React from "react";
import Image from "next/image";
import Countdown from "../components/Countdown";
import Link from "next/link";

const CardWithoutButton = ({
  title = "This is the title",
  subtitle = "this is subtitle",
  image,
  href = "/",
  ...props
}) => {
  return (
    <Link href={href}>
      <div className="relative cursor-pointer">
        <img src={`${image}`} className="w-[250px] h-[280px]" />
        <div className="py-2">
          <p className="text-lg font-medium">{title}</p>
          <p className="text-sm text-color-secondary">{subtitle}</p>
        </div>
        <div className="absolute top-4 left-4">
          <Countdown
            days={props.days}
            hours={props.hours}
            minutes={props.minutes}
          />
        </div>
      </div>
    </Link>
  );
};

export default CardWithoutButton;
