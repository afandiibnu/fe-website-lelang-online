import Link from "next/link";

const ButtonOutline = (props) => {
  return (
    <>
      <Link href={props.href}>
        <button
          className={`text-sm text-center border border-primary ${props.padding} w-full rounded-full text-primary font-medium`}
        >
          {props.text}
        </button>
      </Link>
    </>
  );
};

export default ButtonOutline;
