import { makeAutoObservable } from "mobx";

export class AuthenticationStore {
  isLogin = false;
  ctx;

  constructor(ctx) {
    makeAutoObservable(this);
    this.ctx = ctx;
  }

  setIsLogin(param) {
    this.isLogin = param;
  }
}
