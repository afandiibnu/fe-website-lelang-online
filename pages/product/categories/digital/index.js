import Banner from "../../../../components/Banner";
import CardWithoutButton from "../../../../components/CardWithoutButton";
import Title from "../../../../components/Title";
import App from "../../../../layouts/App";
import { productRepository } from "../../../../repository/product";
import { useState, useEffect } from "react";
import getTimeLeft from "../../../../helper/getTimeLeft";
import getTimeStarted from "../../../../helper/getTimeStarted";
import { parseJwt } from "../../../../helper/DecodeJwt";

const Digital = () => {
  const [product, setProduct] = useState([]);
  const [page, setPage] = useState(1);
  const [limitDigital, setLimitDigital] = useState(10);

  const { data: products } = productRepository.hooks.getProductDigital({
    page,
    limitDigital,
  });

  useEffect(() => {
    if (products !== null && products !== undefined) {
      setProduct(products.items);
    }
  }, [products]);

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const decode = parseJwt(token);

  return (
    <div className="relative">
      <Banner height="h-[450px]">
        <div className="flex justify-center h-full items-center w-full text-center">
          <div className="w-1/3">
            <h1 className="text-5xl font-bold text-white">
              Get the best Digital arts{" "}
              <span className="text-light-green">aero auction</span>
            </h1>
          </div>
        </div>
      </Banner>

      <div className="mx-40 2xl:mx-96 py-28">
        <Title title="Auctions" size="text-2xl" />
        {product.length === 0 && (
          <div className="flex flex-col items-center py-10 rounded  bg-blue-50 mt-4">
            <div>
              <img src="/sedih.png" />
            </div>

            <p className="mt-8 font-bold text-xl">Oh crap!</p>
            <p className="mt-2 text-sm text-color-secondary">
              Sorry, There is no auction for now.
            </p>
          </div>
        )}
        {product.length >= 1 && (
          <div>
            <div className="grid grid-cols-4 gap-10 mt-8">
              {product?.map((i) => {
                const now = new Date();
                const stdate = new Date(i.start_date);
                const enddate = new Date(i.end_date);
                const isOngoing = now >= stdate && now <= enddate;
                const days = getTimeLeft(enddate, "days");
                const minutes = getTimeLeft(enddate, "minutes");
                const hours = getTimeLeft(enddate, "hours");

                const seller = i?.user?.id;
                const login = decode?.result?.id;

                if (isOngoing && i.status === "approve" && seller !== login) {
                  return (
                    <div key={i.id}>
                      <CardWithoutButton
                        image={`http://localhost:3222/product/get-product-image/${i.picture}`}
                        href={`/product/categories/${i.category}/${i.id}`}
                        title={i.name}
                        subtitle={i.description}
                        days={days}
                        hours={hours}
                        minutes={minutes}
                      />
                    </div>
                  );
                }
              })}
            </div>
          </div>
        )}
      </div>
      <div className="mx-40 2xl:mx-96 py-28">
        <div>
          <Title title="Upcoming auction" size="text-2xl" />
          {product.length == 0 && (
            <div className="flex flex-col items-center py-10 rounded  bg-blue-50 mt-4">
              <div>
                <img src="/sedih.png" />
              </div>

              <p className="mt-8 font-bold text-xl">Oh crap!</p>
              <p className="mt-2 text-sm text-color-secondary">
                Sorry, There is no upcoming auction for now.
              </p>
            </div>
          )}
          {product.length >= 1 && (
            <div>
              <div className="grid grid-cols-4 gap-10 mt-8">
                {product?.map((i) => {
                  const now = new Date();
                  const stdate = new Date(i.start_date);
                  const enddate = new Date(i.end_date);
                  const isUpcoming = now <= stdate;
                  const days = getTimeStarted(stdate, "days");
                  const minutes = getTimeStarted(stdate, "minutes");
                  const hours = getTimeStarted(stdate, "hours");

                  const seller = i?.user?.id;
                  const login = decode?.result?.id;

                  if (
                    isUpcoming &&
                    i.status === "approve" &&
                    seller !== login
                  ) {
                    return (
                      <div key={i.id}>
                        <CardWithoutButton
                          image={`http://localhost:3222/product/get-product-image/${i.picture}`}
                          href={`/product/categories/${i.category}/${i.id}`}
                          title={i.name}
                          subtitle={i.description}
                          days={days}
                          hours={hours}
                          minutes={minutes}
                        />
                      </div>
                    );
                  }
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Digital;

Digital.getLayout = (page) => {
  return <App title="Digital arts">{page}</App>;
};
