import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import App from "../../../../layouts/App";
import Title from "../../../../components/Title";
import ButtonPrimary from "../../../../components/ButtonPrimary";
import { productRepository } from "../../../../repository/product";
import getTimeLeft from "../../../../helper/getTimeLeft";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import getTimeStarted from "../../../../helper/getTimeStarted";
import { paymentRepository } from "../../../../repository/payment";
import { bidRepository } from "../../../../repository/bid";
import { toRupiah } from "../../../../helper/toRupiah";

const ProductDetail = () => {
  useAuthenticationPage();
  const [started, setStarted] = useState(true);
  const [insurance, setInsurance] = useState([]);
  const [isPayed, setIsPayed] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const now = new Date();
  const [bidder, setBidder] = useState([]);
  const [highestBid, setHighestBid] = useState("");
  const { data: dataProduct } = productRepository.hooks.getDetail(id);
  const { data: bidderData } = bidRepository.hooks.getCountBidder(id);
  const { data: highestBidData } = bidRepository.hooks.getHighestBid(id);

  const enddate = new Date(dataProduct?.data.end_date);
  const stdate = new Date(dataProduct?.data.start_date);
  const openBid = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
  }).format(dataProduct?.data.open_bid);

  const timeLeft = {
    days: getTimeLeft(enddate, "days"),
    minutes: getTimeLeft(enddate, "minutes"),
    hours: getTimeLeft(enddate, "hours"),
  };

  const timeStarted = {
    days: getTimeStarted(stdate, "days"),
    minutes: getTimeStarted(stdate, "minutes"),
    hours: getTimeStarted(stdate, "hours"),
  };

  const isStarted = () => {
    if (now <= enddate && now >= stdate) {
      return setStarted(started);
    } else if (now <= stdate) {
      return setStarted(!started);
    }
  };

  useEffect(() => {
    isStarted();
  }, [dataProduct?.data]);

  const { data: insuranceData } =
    paymentRepository.hooks.getInsuranceByProductData(id);

  useEffect(() => {
    if (insuranceData !== null && insuranceData !== undefined) {
      setInsurance(insuranceData.data);
    }
    if (bidderData !== null && bidderData !== undefined) {
      setBidder(bidderData.data);
    }
    if (highestBidData !== null && highestBidData !== undefined) {
      setHighestBid(highestBidData.lb_price_bid);
    }
  }, [insuranceData, bidderData, highestBidData]);
  console.log(highestBid);

  useEffect(() => {
    if (insurance?.status === "approve") {
      setIsPayed(true);
    }
  }, [insurance]);

  return (
    <div className="grid grid-cols-2 w-full px-40 py-28 2xl:px-96 gap-x-10">
      <div className="w-full h-[600px] overflow-hidden rounded-3xl ">
        <img
          src={`http://localhost:3222/product/get-product-image/${dataProduct?.data?.picture}`}
          className="w-full"
        />
      </div>
      <div className="flex flex-col p-4 w-full">
        <p className="text-sm font-medium text-color-secondary mb-2 capitalize">
          {dataProduct?.data?.category} art
        </p>
        <h1 className="text-5xl font-bold">{dataProduct?.data?.name}</h1>
        <div className="flex mt-4 gap-6 items-center">
          <p className="text-sm">Start from</p>
          <p className="text-sm font-bold bg-light-green py-[10px] px-6 rounded-full">
            {openBid}
          </p>
        </div>
        <p className="text-sm text-color-secondary mt-6">
          {dataProduct?.data?.description}
        </p>
        <div className="flex mt-6">
          <p className="text-sm text-primary bg-secondary-purple font-bold rounded-full py-2 px-6">
            {dataProduct?.data?.tag}
          </p>
        </div>
        <div className="h-full flex flex-col justify-end">
          {started === true ? (
            <div>
              <div className="mb-40 flex gap-x-6">
                <p>
                  Bidder{" "}
                  <span className="font-medium text-primary">
                    {bidder.length}
                  </span>
                </p>
                <div className="w-[1px] bg-color-secondary"></div>
                <p>
                  Highest Bid{" "}
                  <span className="font-medium text-primary">
                    {toRupiah(highestBid)}
                  </span>
                </p>
              </div>
              <Title title="Auction ended" size="sm" />
              <div className="flex items-center mt-8 justify-between">
                <div className=" flex text-center gap-x-4">
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">{timeLeft.days}</span>
                    <span>DAYS</span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">{timeLeft.hours}</span>
                    <span>HOURS</span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">
                      {timeLeft.minutes}
                    </span>
                    <span>MINUTES</span>
                  </div>
                </div>
                <div>
                  {isPayed ? (
                    <ButtonPrimary
                      href={`/product/bid/${id}`}
                      text="Place a bid"
                      padding="px-10 py-4"
                    />
                  ) : (
                    <ButtonPrimary
                      href={`/user/payment/payment-receipt-insurance/${dataProduct?.data?.id}`}
                      text="Place a bid"
                      padding="px-10 py-4"
                    />
                  )}
                </div>
              </div>
            </div>
          ) : (
            <div>
              <Title title="Auction started" size="sm" />
              <div className="flex items-center mt-8 justify-between">
                <div className=" flex text-center gap-x-4">
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">
                      {timeStarted.days}
                    </span>
                    <span>DAYS</span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">
                      {timeStarted.hours}
                    </span>
                    <span>HOURS</span>
                  </div>
                  <div className="flex flex-col">
                    <span className="text-3xl font-bold">
                      {timeStarted.minutes}
                    </span>
                    <span>MINUTES</span>
                  </div>
                </div>
                <div>
                  <ButtonPrimary
                    href={`/user/payment/payment-receipt-buy/${id}`}
                    text="Buy it now"
                    padding="px-10 py-4"
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default ProductDetail;
ProductDetail.getLayout = (page) => {
  return <App>{page}</App>;
};
