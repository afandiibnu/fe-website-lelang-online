import React, { useEffect, useState } from "react";
import BasicTable from "../../../components/BasicTable";
import App from "../../../layouts/App";
import {
  faCaretDown,
  faCaretUp,
  faInbox,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getHighestBid } from "../../../helper/getHighestBid";
import { bidRepository } from "../../../repository/bid";
import { productRepository } from "../../../repository/product";
import { useRouter } from "next/router";
import { toRupiah } from "../../../helper/toRupiah";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { mutate } from "swr";

const Bid = () => {
  const [password, setPassword] = useState();
  const [product, setProduct] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isNull, setIsNull] = useState(true);
  const [highestBid, setHighestBid] = useState();
  const [myBid, setMyBid] = useState(0);
  const [raise, setRaise] = useState(0);
  const [openBid, setOpenBid] = useState(product?.open_bid);

  const MySwal = withReactContent(Swal);

  const { data: productData } = productRepository.hooks.getDetail(id);

  const productName = product?.name;
  const { data: bidData } = bidRepository.hooks.getLogBid(id);

  useEffect(() => {
    if (productData !== null && productData !== undefined) {
      setProduct(productData?.data);
      setRaise(Number(productData?.data?.auction_raise));
      setOpenBid(Number(productData?.data?.open_bid));
    }

    if (bidData !== null && bidData !== undefined) {
      const tempData = bidData.map((v) => {
        return {
          id: v.id,
          name: v.user.name,
          price_bid: v.price_bid,
          open_bid: v.open_bid,
        };
      });
      setData(tempData);
      setHighestBid(getHighestBid(bidData));
      setMyBid(
        getHighestBid(bidData) + Number(productData?.data?.auction_raise)
      );

      setLoading(false);
      setIsNull(false);
    }
  }, [productData, bidData]);

  const UpHandler = (e) => {
    e.preventDefault();

    setMyBid(myBid + raise);
  };

  const DownHandler = (e) => {
    e.preventDefault();

    if (myBid !== highestBid + raise) {
      setMyBid(myBid - raise);
    } else if (myBid === highestBid + raise) {
      setMyBid(myBid - 0);
    }
  };

  const submitHandler = async (e) => {
    try {
      e.preventDefault();
      const data = {
        price_bid: myBid !== -Infinity ? myBid : openBid,
        password,
      };

      await bidRepository.manipulateData.createLogBid(id, data);
      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Bid success!",
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
      mutate(bidRepository.url.getbyProduct(id));
      router.reload();
    } catch (e) {
      const error = e.response.body.error;
      MySwal.fire({
        toast: true,
        icon: "error",
        title: `${error}`,
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
    }
  };

  const columns = [
    { Header: "Bidder", accessor: "name" },
    { Header: "Bids", accessor: "price_bid" },
  ];

  return (
    <div className="py-28">
      <div className=" mx-96 flex gap-x-10">
        <div className="w-6/12">
          <p className="text-sm font-bold text-color-secondary uppercase">
            {productName}
          </p>
          <div className="flex items-center justify-between mt-10">
            <h1 className="text-2xl font-bold">Highest bid</h1>
            <div className="flex gap-x-4 items-center">
              <p className="px-4 py-2 font-bold bg-light-green rounded-full">
                {highestBid !== -Infinity ? toRupiah(highestBid) : toRupiah(0)}
              </p>
            </div>
          </div>
          <div className="mt-10 rounded-md overflow-hidden border p-6 overflow-y-scroll max-h-[250px]">
            {loading ? (
              <div className="flex justify-center flex-col">
                <FontAwesomeIcon
                  icon={faInbox}
                  className="text-3xl text-primary"
                />
                <p className="text-center mt-4 text-color-secondary">
                  No bids, yet.
                </p>
              </div>
            ) : (
              <BasicTable API={data} cols={columns} />
            )}
          </div>
        </div>
        <div className="w-6/12 rounded-md bg-secondary py-10 px-6">
          <p className="font-bold uppercase text-sm">Your bids</p>
          <form className="mt-6" method="post" onSubmit={submitHandler}>
            <div className="flex justify-between gap-x-2">
              <input
                className="bg-transparent focus:outline-none font-bold w-10/12 text-3xl"
                name="price_bid"
                type="text"
                value={
                  myBid === -Infinity ? toRupiah(openBid) : toRupiah(myBid)
                }
                readOnly="true"
              />
              <div className="w-2/12">
                <button
                  className="text-xl text-primary bg-white px-3 py-1 rounded mr-2"
                  onClick={DownHandler}
                >
                  <FontAwesomeIcon icon={faCaretDown} />
                </button>
                <button
                  className="text-xl text-secondary bg-primary px-3 py-1 rounded"
                  onClick={UpHandler}
                >
                  <FontAwesomeIcon icon={faCaretUp} />
                </button>
              </div>
            </div>
            <div className="mt-40 flex gap-x-4">
              <input
                type="password"
                name="password"
                className="h-[52px] bg-white rounded-full focus:outline-none focus:ring px-4 transition duration-100 w-8/12 placeholder:text-sm"
                placeholder="Type your password"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
              <button
                type="submit"
                className="w-4/12 rounded-full bg-primary text-white text-sm font-bold h-[52px]"
              >
                Bid
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Bid;

Bid.getLayout = (page) => {
  return <App title="Auction name">{page}</App>;
};
