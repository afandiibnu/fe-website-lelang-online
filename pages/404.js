import Image from "next/image";
import Link from "next/link";
import React from "react";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const NotFound = () => {
  return (
    <div className="w-full min-h-screen bg-dark-blue bg-[url('/pattern.png')]">
      <div className="w-full py-6 text-lg px-40 2xl:px-60 flex justify-between mb-[100px]">
        <div className="font-bold text-white">404 Not Found</div>
      </div>
      <div className="px-40 2xl:px-60 flex justify-between">
        <div className="flex justify-center flex-col">
          <h1 className="text-7xl font-bold text-white">Whooops!</h1>
          <p className="w-96 mt-10 text-color-secondary">
            The page you are looking for might be removed or temporarily
            unavailable
          </p>

          <Link href="/">
            <div className="w-52 cursor-pointer px-6 py-4 bg-blue-600 text-center mt-24 rounded-full font-medium text-white">
              <span className="mr-4">Go to main</span>
              <FontAwesomeIcon icon={faArrowRightLong} />
            </div>
          </Link>
        </div>
        <Image src={"/404.svg"} width="560px" height="600px" />
      </div>
    </div>
  );
};

export default NotFound;
