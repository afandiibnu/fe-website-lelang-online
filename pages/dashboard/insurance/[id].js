import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { paymentRepository } from "../../../repository/payment";
import { useRouter } from "next/router";
import { toRupiah } from "../../../helper/toRupiah";

const DetailInsurance = () => {
  const router = useRouter();
  const { id } = router.query;
  const [insurance, setInsurance] = useState([]);

  const { data: insuranceData } = paymentRepository.hooks.getInsuranceById(id);

  // console.log(insuranceData);

  useEffect(() => {
    if (insuranceData !== null && insuranceData !== undefined) {
      setInsurance(insuranceData.data);
    }
  }, [insuranceData]);

  return (
    <div className="mt-10 flex justify-center">
      <div className="bg-white flex w-7/12 gap-x-6 px-10 py-6 rounded-md">
        <div className=" w-full">
          <div className=" overflow-hidden rounded">
            <img
              src={`http://localhost:3222/insurance-money/get-insurance-image/${insurance.proof_of_payment}`}
            />
          </div>
        </div>
        <div className="w-full">
          <p className="font-semibold text-sm mb-3 text-primary">Payment ID</p>
          <p className="mb-10 text-color-secondary">{insurance?.id}</p>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Buyer Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Buyer name</p>
                <p>Phone number</p>
                <p>Address</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p className="text-end">{insurance?.user?.name}</p>
                <p className="text-end">
                  {insurance?.user?.phone_number
                    ? insurance?.user?.phone_number
                    : "-"}
                </p>
                <p className="text-end">
                  {insurance?.user?.addres ? insurance?.user?.addres : "-"}
                </p>
              </div>
            </div>
          </div>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Product Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Product name</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p>{insurance?.product?.name}</p>
              </div>
            </div>
          </div>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Payment Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Price</p>
                <p>Payment date</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p className="text-end">
                  {toRupiah(parseInt(insurance?.price))}
                </p>
                <p className="text-end">{insurance?.purchase_date}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailInsurance;

DetailInsurance.getLayout = (page) => {
  return <Dashboard>{page}</Dashboard>;
};
