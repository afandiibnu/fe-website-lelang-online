import React, { useEffect, useState } from "react";
import { userRepository } from "../../../repository/user";
import Dashboard from "../../../layouts/Dashboard";
import AdminUserTable from "../../../components/UserAdminTable";
import { paymentRepository } from "../../../repository/payment";
import InsurancePaymentTable from "../../../components/InsurancePaymentTable";
import SearchAdmin from "../../../components/SearchAdmin";
import Link from "next/link";
import { mutate } from "swr";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { useRouter } from "next/router";

const Insurance = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const MySwal = withReactContent(Swal);
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const { id } = router.query;

  const { data: payment } = paymentRepository.hooks.getAllInsurance({
    page,
    limit,
  });
  const nextHandler = (e) => {
    e.preventDefault();

    if (data.length == 0) {
      setPage(page + 0);
    } else if (data.length % limit <= 0) {
      setPage(page + 1);
    }
  };

  const previousHandler = (e) => {
    e.preventDefault();

    if (page == 1) {
      setPage(page - 0);
    } else if (page > 1) {
      setPage(page - 1);
    }
  };

  const updatePaymentInsurance = async (id) => {
    try {
      const data = { status: "approve" };
      MySwal.fire({
        icon: "warning",
        iconColor: "#315AFE",
        title: (
          <p className="text-xl mb-6">
            Are you sure want to approve insurance payment?
          </p>
        ),
        position: "center",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Yes, approve",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#315AFE",
      }).then(async (result) => {
        if (result.isConfirmed) {
          await paymentRepository.manipulateData.approveInsurancePayment(
            id,
            data
          );
          await mutate(paymentRepository.url.getAllInsurance({ page, limit }));
          MySwal.fire({
            toast: true,
            icon: "success",
            title: "Approve insurance payment success",
            position: "top",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      });
    } catch (e) {
      console.log(e.response);
    }
  };

  const rejectPaymentInsurance = async (id) => {
    try {
      const data = { status: "rejected" };

      MySwal.fire({
        icon: "warning",
        title: (
          <p className="text-xl mb-6">
            Are you sure want to reject insurance payment?
          </p>
        ),
        position: "center",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Yes, reject",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#315AFE",
      }).then(async (result) => {
        if (result.isConfirmed) {
          await paymentRepository.manipulateData.rejectedInsurancePayment(
            id,
            data
          );
          await mutate(paymentRepository.url.getAllInsurance({ page, limit }));
          MySwal.fire({
            toast: true,
            icon: "success",
            title: "Reject insurance payment success",
            position: "top",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    if (payment !== null && payment !== undefined) {
      const tempData = payment?.items?.map((v) => {
        const change = new Date(v.purchase_date);
        const date =
          change.getFullYear() +
          "-" +
          (change.getMonth() + 1) +
          "-" +
          change.getDate();
        return {
          id: v?.id,
          userId: v?.user?.id,
          name: v?.user?.name,
          product: v?.product?.name,
          price: v?.price,
          status: v?.status,
          purchase_date: date,
        };
      });

      setData(tempData);
      setLoading(false);
    }
  }, [payment]);

  const columns = [
    { Header: "Name", accessor: "name" },
    { Header: "Product", accessor: "product" },
    { Header: "Price", accessor: "price" },
    { Header: "Status", accessor: "status" },
    { Header: "Date", accessor: "purchase_date" },
  ];

  return (
    <div className="w-full py-8">
      {loading ? (
        <div>loading...</div>
      ) : (
        <div className="bg-white p-10 w-full">
          <div>
            <table className="w-full text-sm table-auto mt-6">
              <thead className="">
                <tr className="bg-secondary">
                  <td className="font-medium px-4 py-4">Product</td>
                  <td className="font-medium px-4 py-4">Bidder Name</td>
                  <td className="font-medium px-4 py-4">Price</td>
                  <td className="font-medium px-4 py-4">Status</td>
                  <td className="font-medium px-4 py-4">Date</td>
                  <td className="font-medium px-4 py-4">Actions</td>
                </tr>
              </thead>
              <tbody>
                {data?.map((v) => {
                  let style;
                  if (v.status == "approve") {
                    style = "text-green-500 ";
                  }
                  if (v.status == "rejected") {
                    style = "text-red-500 ";
                  }
                  if (v.status == "pending") {
                    style = "text-yellow-500 ";
                  }
                  return (
                    <tr key={v.id}>
                      <td className="px-4 py-6">{v.product}</td>
                      <td className="px-4 py-6">{v.name}</td>
                      <td className="px-4 py-6">{v.price}</td>
                      <td className={`px-4 py-8 `}>
                        <span className={style}>{v.status}</span>
                      </td>
                      <td className="px-4 py-6">{v.purchase_date}</td>

                      <td className="">
                        <div className="flex gap-x-4">
                          <button
                            className="text-sm text-center bg-green-100 px-3 py-1.5 rounded-full text-green-700 font-medium hover:shadow-md hover:shadow-green-600/20 transiton duration-150"
                            type="submit"
                            onClick={() => {
                              updatePaymentInsurance(v.id);
                            }}
                          >
                            Approve
                          </button>
                          <div>
                            <Link href={`/dashboard/insurance/${v.id}`}>
                              <button
                                className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                                type="submit"
                              >
                                detail
                              </button>
                            </Link>
                          </div>
                          <div>
                            <button
                              className="text-sm text-center bg-red-100 px-3 py-1.5 rounded-full text-red-700 font-medium hover:shadow-md hover:shadow-red-600/20 transiton duration-150"
                              type="submit"
                              onClick={() => {
                                rejectPaymentInsurance(v.id);
                              }}
                            >
                              Rejected
                            </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className="flex justify-end gap-x-2 mt-10">
              <div>
                <button
                  onClick={previousHandler}
                  className="py-1 px-2 bg-secondary-purple text-primary rounded"
                >
                  <FontAwesomeIcon icon={faChevronLeft} />
                </button>
              </div>
              <div className="py-1 px-3 bg-primary text-white rounded">
                {page}
              </div>
              <button
                onClick={nextHandler}
                className="py-1 px-2 bg-secondary-purple text-primary rounded"
              >
                <FontAwesomeIcon icon={faChevronRight} />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Insurance;

Insurance.getLayout = (page) => {
  return <Dashboard title="Insurance">{page}</Dashboard>;
};
