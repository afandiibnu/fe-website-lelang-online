import React, { useEffect, useState } from "react";
import { userRepository } from "../../../repository/user";
import Dashboard from "../../../layouts/Dashboard";
import AdminUserTable from "../../../components/UserAdminTable";
import { mutate } from "swr";
import { data } from "autoprefixer";
import SearchAdmin from "../../../components/SearchAdmin";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrash,
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { SearchOutlined } from "@ant-design/icons";

const User = () => {
  const [api, setAPI] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const MySwal = withReactContent(Swal);

  const { data: user } = userRepository.hooks.getallUser({
    page,
    limit,
    search,
  });

  useEffect(() => {
    if (user !== null && user !== undefined) {
      const tempData = user?.items?.map((v) => {
        return {
          id: v.id,
          Name: v.name,
          picture: v.picture,
          email: v.account.email,
          nik: v.nik,
          phone_number: v.phone_number,
          level: v.account.role.description,
        };
      });
      setAPI(tempData);
      setLoading(false);
    }
  }, [user]);

  const nextHandler = (e) => {
    e.preventDefault();

    if (api.length == 0) {
      setPage(page + 0);
    } else if (api.length % limit <= 0) {
      setPage(page + 1);
    }
  };

  const DeleteUser = async (id) => {
    try {
      const data = { deletedAt: Date.now() };
      MySwal.fire({
        toast: false,
        icon: "warning",
        title: "Are you sure want to delete?",
        text: "You won't be able to revert this!",
        position: "center",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#ef4444",
      }).then(async (result) => {
        if (result.isConfirmed) {
          await userRepository.manipulateData.deleteUser(data, id);
          mutate(userRepository.url.user({ page, limit, search }));
          MySwal.fire({
            toast: true,
            icon: "success",
            title: "Delete user success",
            position: "top",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      });
    } catch (e) {
      console.log(e.response);
    }
  };

  const previousHandler = (e) => {
    e.preventDefault();

    if (page == 1) {
      setPage(page - 0);
    } else if (page > 1) {
      setPage(page - 1);
    }
  };

  return (
    <div className="w-full py-8">
      {loading ? (
        <div>loading...</div>
      ) : (
        <div className="bg-white rounded-lg p-10">
          <div className="pb-6">
            <div className="relative w-full">
              <label className="">
                <span className="absolute inset-y-0 left-4 flex items-center text-color-secondary text-md">
                  <SearchOutlined></SearchOutlined>
                </span>
                <input
                  className="placeholder:text-color-secondary h-12 block rounded py-2 pl-10 pr-3 focus:outline-none text-sm  w-full focus:ring-2 focus:shadow-md focus:shadow-primary/20 transition duration-100 bg-secondary"
                  placeholder="Search....."
                  type="text"
                  name="search"
                  autoComplete="off"
                  onChange={(e) => setSearch(e.target.value)}
                />
              </label>
            </div>
          </div>

          <div>
            <div className="bg-secondary w-full h-[1px]"></div>
            <table className="w-full text-sm table-auto mt-6">
              <thead className="">
                <tr className="bg-secondary">
                  <td className="font-medium px-4 py-4" colSpan={2}>
                    Name
                  </td>
                  <td className="font-medium px-4 py-4">Email</td>
                  <td className="font-medium px-4 py-4">NIK</td>
                  <td className="font-medium px-4 py-4">Phone number</td>
                  <td className="font-medium px-4 py-4">level</td>
                  <td className="font-medium px-4 py-4">Actions</td>
                </tr>
              </thead>
              <tbody>
                {api?.map((v) => {
                  const status = v.level;

                  if (status != "Admin") {
                    return (
                      <tr key={v.id}>
                        <td>
                          <div className="border rounded-full w-12 h-12 overflow-hidden">
                            <img
                              src={`http://localhost:3222/users/get-profile-image/${v.picture}`}
                            />
                          </div>
                        </td>
                        <td className="px-4 py-6">
                          {v.Name == null ? "Null" : v.Name}
                        </td>
                        <td className="px-4 py-6">{v.email}</td>
                        <td className="px-4 py-6">
                          {v.nik == null ? "-" : v.nik}
                        </td>
                        <td className="px-4 py-6 ">
                          {v.phone_number == null ? "-" : v.phone_number}
                        </td>
                        <td className="px-4 py-6">{v.level}</td>

                        <td className="px-4 py-6 flex gap-x-2">
                          <Link href={`/dashboard/user/${v.id}`}>
                            <div className="px-4 py-2 rounded-full cursor-pointer  text-primary text-sm bg-secondary">
                              Detail
                            </div>
                          </Link>
                          <div>
                            <button
                              onClick={() => DeleteUser(v.id)}
                              className="px-4 py-2 rounded-full text-red-700 text-sm bg-red-100"
                            >
                              Delete
                            </button>
                          </div>
                        </td>
                      </tr>
                    );
                  }
                })}
              </tbody>
            </table>
            <div className="flex justify-end gap-x-2 mt-10">
              <div>
                <button
                  onClick={previousHandler}
                  className="py-1 px-2 bg-secondary-purple text-primary rounded"
                >
                  <FontAwesomeIcon icon={faChevronLeft} />
                </button>
              </div>
              <div className="py-1 px-3 bg-primary text-white rounded">
                {page}
              </div>
              <button
                onClick={nextHandler}
                className="py-1 px-2 bg-secondary-purple text-primary rounded"
              >
                <FontAwesomeIcon icon={faChevronRight} />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default User;

User.getLayout = (page) => {
  return <Dashboard title="User">{page}</Dashboard>;
};
