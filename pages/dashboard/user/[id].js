import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { productRepository } from "../../../repository/product";
import { userRepository } from "../../../repository/user";
import Link from "next/link";

const DetailUser = () => {
  const [user, setUser] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const { data: userData } = userRepository.hooks.getDetail(id);

  const [products, setProduct] = useState([]);
  const { data: product } = productRepository.hooks.getMyAuctionAdmin(id);

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData.data);
    }
  }, [userData]);

  useEffect(() => {
    if (product !== null && product !== undefined) {
      setProduct(product.data);
    }
  }, [product]);

  return (
    <div className="mt-10  h-full  ">
      <div className="flex justify-between gap-x-10">
        <div className="w-full bg-white rounded-lg px-8 py-10">
          <div className="flex items-center gap-x-6 bg-primary rounded p-4">
            <div className="rounded-full overflow-hidden">
              <img
                src={`http://localhost:3222/users/get-profile-image/${user.picture}`}
              />
            </div>
            <div className="flex flex-col gap-y-4">
              <p className="text-xl font-bold text-white capitalize">
                {user?.name}
              </p>
              <p className="font-bold text-center text-xs px-4 py-2 rounded-full bg-light-green">
                {user?.account?.role?.description}
              </p>
            </div>
          </div>
          {/* profile */}
          <div className="mt-10">
            <p className="font-medium text-base mb-4">Profile</p>

            {/* name */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">Name</div>
              <div>{user?.name}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />

            {/* nik */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">NIK</div>
              <div>{user?.nik == null ? "-" : user?.nik}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />

            {/* phone number */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">
                Phone number
              </div>
              <div>{user?.phone_number == null ? "-" : user?.phone_number}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />

            {/* bank account number */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">
                Bank account number
              </div>
              <div>
                {user.bank_account_number == null
                  ? "-"
                  : user.bank_account_number}
              </div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />

            {/* address */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">Address</div>
              <div>{user?.addres == null ? "-" : user?.addres}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />

            {/* postal code */}
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">
                Postal Code
              </div>
              <div>{user?.postal_code == null ? "-" : user?.postal_code}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2 mb-6" />
          </div>

          {/* account */}
          <div className="mt-10">
            <p className="font-medium text-base mb-4">Account</p>
            <div className="flex justify-between">
              <div className="text-medium text-color-secondary">Email</div>
              <div>{userData?.data?.account?.email}</div>
            </div>
            <div className="w-full h-[1px] bg-secondary mt-2" />
          </div>
        </div>

        <div className="w-full p-4 bg-white rounded-lg px-8 py-10">
          <p className="font-bold text-lg">Auction</p>
          {products.length === 0 ? (
            <div className="flex flex-col items-center mt-10">
              <img src="/3.svg" className="w-80"></img>
              <p className="mt-8 text-lg text-color-secondary">
                No auction, yet.
              </p>
            </div>
          ) : (
            <div className="flex flex-wrap mt-8 gap-6 ">
              {products?.map((v) => {
                let status;
                let text;
                let bg;

                const now = new Date();
                const start_date = new Date(v.start_date);
                const end_date = new Date(v.end_date);

                if (now >= start_date && now <= end_date) {
                  status = "ongoing";
                  text = "text-green-700";
                  bg = "bg-green-100";
                }
                if (now <= start_date) {
                  status = "upcoming";
                  text = "text-yellow-700";
                  bg = "bg-yellow-100";
                }
                if (now >= end_date) {
                  status = "Finished";
                  text = "text-red-700";
                  bg = "bg-red-100";
                }

                return (
                  <Link href={`/dashboard/product/${v.id}`}>
                    <div
                      className=" bg-white rounded p-2 drop-shadow-2xl cursor-pointer"
                      key={v.id}
                    >
                      <div
                        className={`rounded h-60 overflow-hidden min-w-[300px] max-w-[300px] relative`}
                      >
                        <div
                          className={`absolute bottom-2 right-2 py-1 px-3 text-xs rounded-full ${text} ${bg}`}
                        >
                          {status}
                        </div>
                        <img
                          src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                          className="w-full "
                        />
                      </div>
                      <div className="mt-4">
                        <p className="text-xs capitalize text-color-secondary">
                          {v.category}
                        </p>
                        <p className="font-medium capitalize mt-1">{v.name}</p>
                        <p>{}</p>
                      </div>
                    </div>
                  </Link>
                );
              })}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default DetailUser;

DetailUser.getLayout = (page) => {
  return <Dashboard title="User detail">{page}</Dashboard>;
};
