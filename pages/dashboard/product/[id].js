import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { productRepository } from "../../../repository/product";
import { toRupiah } from "../../../helper/toRupiah";

const DetailProduct = () => {
  const [data, setData] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const { data: product } = productRepository.hooks.getDetail(id);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  useEffect(() => {
    if (product !== null && product !== undefined) {
      setData(product.data);
    }
    setStartDate(
      new Date(product?.data?.start_date).toLocaleDateString("id-ID", {
        timeZone: "Asia/Jakarta",
        hour: "2-digit",
        minute: "2-digit",
      })
    );
    setEndDate(
      new Date(product?.data?.end_date).toLocaleDateString("id-ID", {
        timeZone: "Asia/Jakarta",
        hour: "2-digit",
        minute: "2-digit",
      })
    );
  }, [product]);
  return (
    <div className="mt-10 bg-white rounded-md flex p-6 ">
      <div className="w-full flex gap-x-10">
        <div className="w-[600px]  bg-secondary rounded-lg overflow-hidden mb-4 relative">
          <div className="absolute bottom-4 right-4 flex gap-x-2">
            <div className="px-6 py-2 rounded-full bg-primary text-white capitalize  drop-shadow-lg text-sm">
              #{data?.tag}
            </div>
          </div>
          <img
            src={`http://localhost:3222/product/get-product-image/${data.picture}`}
            className="object-none w-full object-center max-h-[500px]"
          />
        </div>
        <div className="">
          <div className="">
            <p className="text-md font-medium capitalize text-color-secondary">
              {data?.category}
            </p>
            <p className="font-bold text-3xl">{data?.name}</p>
            <p className="text-xs leading-relaxed mt-2 text-color-secondary">
              {data?.description}
            </p>
          </div>

          <p className="mt-6 mb-2 text-primary font-medium">Price</p>
          <div className="flex justify-between gap-x-6 rounded text-sm bg-secondary p-4">
            <div className="">
              <p className="font-medium ">Open bid</p>
              {toRupiah(data?.open_bid)}
            </div>
            <div className="">
              <p className="font-medium mb-2">Auction raise</p>
              {toRupiah(data?.auction_raise)}
            </div>
            <div className="">
              <p className="font-medium mb-2">Buy it now</p>
              {toRupiah(data?.buy_price)}
            </div>
          </div>
          <p className="mt-6 mb-2 text-primary font-medium">Date</p>
          <div className="flex gap-x-6 rounded text-sm bg-secondary p-4">
            <div className="mr-2">
              <p className="font-medium mb-2">Start date</p>
              {startDate}
            </div>
            <div className="">
              <p className=" font-medium mb-2">End date</p>
              {endDate}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailProduct;

DetailProduct.getLayout = (page) => {
  return <Dashboard>{page}</Dashboard>;
};
