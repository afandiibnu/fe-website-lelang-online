import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { productRepository } from "../../../repository/product";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { mutate } from "swr";
import { SearchOutlined } from "@ant-design/icons";
import { toRupiah } from "../../../helper/toRupiah";

const Product = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);
  const MySwal = withReactContent(Swal);

  const { data: product } = productRepository.hooks.getSearch({
    page,
    limit,
    search,
  });

  // console.log(data);
  useEffect(() => {
    if (product !== null && product !== undefined) {
      const tempData = product?.items?.map((v) => {
        return {
          id: v.id,
          product: v.name,
          status: v.status,
          picture: v.picture,
          name: v.user.name,
          tag: v.tag,
          category: v.category,
          open_bid: v.open_bid,
          buy_price: v.buy_price,
          admin_name: v.admin_name,
          buyed: v.buyed,
        };
      });

      setData(tempData);
      setLoading(false);
    }
  }, [product]);

  const approveProduct = async (id) => {
    try {
      const data = { status: "approve" };

      MySwal.fire({
        icon: "warning",
        iconColor: "#315AFE",
        title: (
          <p className="text-xl mb-6">Are you sure want to approve product?</p>
        ),
        position: "center",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Yes, approve",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#315AFE",
      }).then(async (result) => {
        if (result.isConfirmed) {
          await productRepository.manipulateData.approveProduct(id, data);
          await mutate(
            productRepository.url.searchProduct({ limit, page, search })
          );
          MySwal.fire({
            toast: true,
            icon: "success",
            title: "Approve product success",
            position: "top",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      });
    } catch (e) {
      console.log(e.response);
    }
  };

  const rejectProduct = async (id) => {
    try {
      const data = { status: "rejected" };

      MySwal.fire({
        icon: "warning",
        title: (
          <p className="text-xl mb-6">Are you sure want to reject product?</p>
        ),
        position: "center",
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Yes, Reject",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#ef4444",
      }).then(async (result) => {
        if (result.isConfirmed) {
          await productRepository.manipulateData.rejectProduct(id, data);
          await mutate(
            productRepository.url.searchProduct({ limit, page, search })
          );
          MySwal.fire({
            toast: true,
            icon: "success",
            title: "Reject product success",
            position: "top",
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
          });
        }
      });
    } catch (e) {
      console.log(e);
    }
  };

  const nextHandler = (e) => {
    e.preventDefault();

    if (data.length == 0) {
      setPage(page + 0);
    } else if (data.length % limit <= 0) {
      setPage(page + 1);
    }
  };

  const previousHandler = (e) => {
    e.preventDefault();

    if (page == 1) {
      setPage(page - 0);
    } else if (page > 1) {
      setPage(page - 1);
    }
  };

  return (
    <div className="bg-white rounded-lg mt-8">
      {loading ? (
        <div>loading...</div>
      ) : (
        <div className="bg-white p-10 ">
          <div className="mb-4 flex justify-end">
            <Link href={`http://localhost:3222/report/download/exel`}>
              <button className="text-sm bg-primary text-white font-medium px-4 py-2 h-full rounded ">
                Export to excel
              </button>
            </Link>
          </div>
          <div className="pb-3">
            <div className="relative w-full">
              <label className="">
                <span className="absolute inset-y-0 left-4 flex items-center text-color-secondary text-md">
                  <SearchOutlined></SearchOutlined>
                </span>
                <input
                  className="placeholder:text-color-secondary h-12 block rounded py-2 pl-10 pr-3 focus:outline-none text-sm  w-full focus:ring-2 focus:shadow-md focus:shadow-primary/20 transition duration-100 bg-secondary"
                  placeholder="Search....."
                  type="text"
                  name="search"
                  autoComplete="off"
                  onChange={(e) => setSearch(e.target.value)}
                />
              </label>
            </div>
          </div>

          <div>
            <div className="bg-secondary w-full h-[1px]"></div>
            <table className="w-full text-sm table-auto mt-6">
              <thead className="">
                <tr className="bg-secondary">
                  <td className="font-medium px-4 py-4" colSpan={2}>
                    Product
                  </td>
                  <td className="font-medium px-4 py-4">Auctioner</td>
                  <td className="font-medium px-4 py-4">Category</td>
                  <td className="font-medium px-4 py-4">Tags</td>
                  <td className="font-medium px-4 py-4">OpenBid</td>
                  <td className="font-medium px-4 py-4">Buy Price</td>
                  <td className="font-medium px-4 py-4">Status</td>
                  <td className="font-medium px-4 py-4">Admin Name</td>
                  <td className="font-medium w-10">Actions</td>
                </tr>
              </thead>
              <tbody>
                {data?.map((v) => {
                  let style;
                  let text;
                  if (v.status == "approve") {
                    style = "text-green-500 ";
                  }
                  if (v.status == "rejected") {
                    style = "text-red-500 ";
                  }
                  if (v.status == "pending") {
                    style = "text-yellow-500 ";
                  }
                  if (v.buyed == true) {
                    text = "Sold out";
                    style = "text-red-500 ";
                  }
                  return (
                    <tr key={v.id}>
                      <td>
                        <div className="border rounded-full w-12 h-12 overflow-hidden ">
                          <img
                            src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                          />
                        </div>
                      </td>
                      <td className="px-4 py-8">{v.product}</td>
                      <td className="px-4 py-8">{v.name}</td>
                      <td className="px-4 py-8">{v.category}</td>
                      <td className="px-4 py-8">{v.tag}</td>
                      <td className="px-4 py-8">{toRupiah(v.open_bid)}</td>
                      <td className="px-4 py-8">{toRupiah(v.buy_price)}</td>
                      <td className={`px-4 py-8 `}>
                        <span className={style}>
                          {v.buyed == true ? text : v.status}
                        </span>
                      </td>
                      <td className="px-4 py-8">{v.admin_name}</td>
                      <td className="py-3">
                        <div className="flex gap-x-4">
                          <button
                            className="text-sm text-center bg-green-100 px-3 py-1.5 rounded-full text-green-700 font-medium hover:shadow-md hover:shadow-green-600/20 transiton duration-150"
                            type="submit"
                            onClick={() => approveProduct(v.id)}
                          >
                            Approve
                          </button>
                          <div>
                            <Link href={`/dashboard/product/${v.id}`}>
                              <button
                                className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                                type="submit"
                              >
                                detail
                              </button>
                            </Link>
                          </div>
                          <div>
                            <button
                              className="text-sm text-center bg-red-100 px-3 py-1.5 rounded-full text-red-700 font-medium hover:shadow-md hover:shadow-red-600/20 transiton duration-150"
                              type="submit"
                              onClick={() => rejectProduct(v.id)}
                            >
                              Reject
                            </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className="flex justify-end gap-x-2 mt-10">
              <div>
                <button
                  onClick={previousHandler}
                  className="py-1 px-2 bg-secondary-purple text-primary rounded"
                >
                  <FontAwesomeIcon icon={faChevronLeft} />
                </button>
              </div>
              <div className="py-1 px-3 bg-primary text-white rounded">
                {page}
              </div>
              <button
                onClick={nextHandler}
                className="py-1 px-2 bg-secondary-purple text-primary rounded"
              >
                <FontAwesomeIcon icon={faChevronRight} />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Product;

Product.getLayout = (page) => {
  return <Dashboard title="Product">{page}</Dashboard>;
};
