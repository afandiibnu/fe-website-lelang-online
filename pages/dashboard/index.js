import React from "react";
import { useEffect, useState } from "react";
import Dashboard from "../../layouts/Dashboard";
import { bidRepository } from "../../repository/bid";
import { userRepository } from "../../repository/user";
import { productRepository } from "/repository/product";

const HomeDashboard = () => {
  const [productFines, setProductsFine] = useState([]);
  const { data: product } = productRepository.hooks.getFineArt();

  const [productDigitals, setProductDigitals] = useState([]);
  const { data: productD } = productRepository.hooks.getDigitalArt();

  const [bidders, setBidders] = useState([]);
  const { data: bidder } = userRepository.hooks.getBidder();

  const [auctioners, setAuctioners] = useState([]);
  const { data: auctioner } = userRepository.hooks.getAuctioner();

  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(4);
  const { data: productst } = productRepository.hooks.getAllProduct({
    page,
    limit,
  });

  const [user, setUser] = useState([]);
  const [search, setsearch] = useState("");
  const { data: users } = userRepository.hooks.getallUser({
    page,
    limit,
    search,
  });

  useEffect(() => {
    if (users !== null && users !== undefined) {
      setUser(users.items);
    }
  }, [users]);

  useEffect(() => {
    if (productst !== null && productst !== undefined) {
      setProducts(productst.items);
    }
  }, [productst]);

  useEffect(() => {
    if (product !== null && product !== undefined) {
      setProductsFine(product.data);
    }
  }, [product]);

  useEffect(() => {
    if (productD !== null && productD !== undefined) {
      setProductDigitals(productD.data);
    }
  }, [productD]);

  useEffect(() => {
    if (bidder !== null && bidder !== undefined) {
      setBidders(bidder);
    }
  }, [bidder]);

  useEffect(() => {
    if (auctioner !== null && auctioner !== undefined) {
      setAuctioners(auctioner);
    }
  }, [auctioner]);
  let i = 1;

  // console.log(user);
  return (
    <div className="mt-4 flex justify-between gap-x-10 ">
      <div className="w-full flex flex-col ">
        <div className=" w-full rounded-lg  bg-white p-10 shadow-xl shadow-primary/5 divide-y mb-5">
          <div className="pb-4">
            <p className="text font-bold mb-2 text-primary">Products</p>
            <p className="text-sm text-color-secondary">This is product list</p>
          </div>
          <div className="pt-4 text-sm">
            <table>
              <thead>
                <tr className="">
                  <td className="pr-96 py-2 ">
                    <p className="font-semibold ">Product Name</p>
                  </td>
                  <td>
                    <p className=" font-semibold">User Name</p>
                  </td>
                </tr>
              </thead>
              <tbody>
                {products?.map((v) => {
                  return (
                    <tr>
                      <td className="py-2">
                        <p>{v.name == "-" ? "Null" : v.name}</p>
                      </td>
                      <td>
                        <p>{v.user.name == "-" ? "Null " : v.user.name}</p>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div className=" w-full  rounded-lg  bg-white p-10 shadow-xl shadow-primary/5 divide-y">
          <div className="pb-3">
            <p className="font-bold text-primary mb-2">Users</p>
            <p className="text-sm text-color-secondary">This is users list</p>
          </div>
          <div className="pt-4 text-sm">
            <table>
              <thead>
                <tr className="">
                  <td className="pr-80 py-2 ">
                    <p className="font-semibold">User Name</p>
                  </td>
                  <td>
                    <p className="font-semibold">Role</p>
                  </td>
                </tr>
              </thead>

              <tbody>
                {user?.map((v) => {
                  return (
                    <tr>
                      <td className="py-2">
                        <p>{v.name == null ? "Null" : v.name}</p>
                      </td>
                      <td>
                        <p className=" ">
                          {v.account.role.description == null
                            ? "Null"
                            : v?.account?.role.description}
                        </p>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div className="flex flex-col gap-y-20 w-full">
        <div className="grid grid-cols-2 gap-4 place-items-stretch">
          <div className="rounded-md flex justify-between items-center bg-blue-600 px-4 py-5 shadow-xl shadow-primary/5">
            <div>
              <p className="text-sm font-medium text-white">Auctions</p>
              <p className="text-sm text-white font-bold">
                {auctioners[0]?.length}
              </p>
            </div>
            <div>
              <img src="/groupPeople1.png"></img>
            </div>
          </div>
          <div className="rounded-md flex justify-between items-center bg-white px-4 py-5 shadow-xl shadow-primary/5">
            <div>
              <p className="text-sm font-medium text-black">Digital Art</p>
              <p className="text-sm text-black font-bold">
                {productDigitals[0]?.length}
              </p>
            </div>
            <div>
              <img src="/groupPeople2.png"></img>
            </div>
          </div>

          <div className="rounded-md flex justify-between items-center bg-white px-4 py-5 shadow-xl shadow-primary/5">
            <div>
              <p className="text-sm font-medium text-black">Bidder</p>
              <p className="text-sm text-black font-bold">
                {bidders[0]?.length}
              </p>
            </div>
            <div>
              <img src="/groupPeople3.png"></img>
            </div>
          </div>

          <div className=" rounded-md flex justify-between items-center bg-white px-4 py-5 shadow-xl shadow-primary/5">
            <div>
              <p className="text-sm font-medium text-black">Fine Art</p>
              <p className="text-sm text-black font-bold">
                {productFines[0]?.length}
              </p>
            </div>
            <div>
              <img src="/groupPeople4.png"></img>
            </div>
          </div>
        </div>

        <div>
          <div className="font-bold text-[#315AFE] text-md">Auction</div>
          <div className="text-sm text-color-secondary">
            Here is Auction list
          </div>
          <div className="mt-4">
            <table className="font-medium text-sm w-full border-8  border-secondary ">
              <thead>
                <tr className="font-bold text-md leading-10">
                  <td className="pr-8 p-4 "> #</td>
                  <td className="pr-8 p-4">Image</td>
                  <td className="pr-8 p-4">Auction name</td>
                  <td className="pr-8 p-4">Type</td>
                  <td className="pr-8 p-4">Status</td>
                </tr>
              </thead>

              <tbody className="border">
                {products?.map((v) => {
                  return (
                    <tr className="bg-white border-8  border-secondary ">
                      <td className="pr-8 p-4">{i++}</td>
                      <td className="pr-8 p-4">
                        <div className="w-12 h-12 rounded-full overflow-hidden">
                          <img
                            src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                          />
                        </div>
                      </td>
                      <td className="pr-8 p-4">{v.name}</td>
                      <td className="pr-8 p-4">{v.category}</td>
                      <td className="pr-8 p-4 ">{v.status}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeDashboard;

HomeDashboard.getLayout = (page) => {
  return <Dashboard title="Dashboard">{page}</Dashboard>;
};
