import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { paymentRepository } from "../../../repository/payment";
import { useRouter } from "next/router";
import { toRupiah } from "../../../helper/toRupiah";

const DetailPayment = () => {
  const [payment, setPayment] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const { data: paymentData } = paymentRepository.hooks.getPaymentDetail(id);

  useEffect(() => {
    if (paymentData !== null && paymentData !== undefined) {
      setPayment(paymentData.data[0]);
    }
  }, [paymentData]);

  return (
    <div className="mt-10 flex justify-center">
      <div className="bg-white flex w-7/12 gap-x-6 px-10 py-6 rounded-md">
        <div className="w-full flex justify-center items-center">
          <div className=" overflow-hidden rounded">
            {payment?.status == null ||
            payment?.status == "" ||
            payment?.status == undefined ? (
              <div className="w-full">Payment is unpaid</div>
            ) : (
              <img
                src={`http://localhost:3222/payments/get-payment-image/${payment?.proof_of_payment}`}
              />
            )}
          </div>
        </div>
        <div className="w-full">
          <p className="font-semibold text-sm mb-3 text-primary">Payment ID</p>
          <p className="mb-10 text-color-secondary">{payment?.id}</p>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Buyer Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Buyer name</p>
                <p>Phone number</p>
                <p>Address</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p className="text-end">{payment?.user?.name}</p>
                <p className="text-end">
                  {payment?.user?.phone_number
                    ? payment?.user?.phone_number
                    : "-"}
                </p>
                <p className="text-end">
                  {payment?.user?.addres ? payment?.user?.addres : "-"}
                </p>
              </div>
            </div>
          </div>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Product Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Product name</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p>{payment?.product?.name}</p>
              </div>
            </div>
          </div>
          <div className="mb-10">
            <p className="font-semibold text-primary text-sm">Payment Info</p>
            <div className="mt-3 flex text-base justify-between">
              <div className="font-medium flex flex-col gap-y-2">
                <p>Price</p>
                <p>Payment date</p>
              </div>
              <div className="flex flex-col gap-y-2">
                <p className="text-end">
                  {toRupiah(parseInt(payment?.fixed_price))}
                </p>
                <p className="text-end">{payment?.purchase_date}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailPayment;

DetailPayment.getLayout = (page) => {
  return <Dashboard title="Payment details">{page}</Dashboard>;
};
