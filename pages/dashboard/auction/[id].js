import React, { useEffect, useState } from "react";
import Dashboard from "../../../layouts/Dashboard";
import { bidRepository } from "../../../repository/bid";
import { productRepository } from "../../../repository/product";
import { useRouter } from "next/router";
import { toRupiah } from "../../../helper/toRupiah";
import { mutate } from "swr";

export default function DetailAuction() {
  const [bidData, setBidData] = useState([]);
  const [auctionDetail, setAuctionDetail] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const router = useRouter();
  const { id } = router.query;

  const { data: BidData } = bidRepository.hooks.getLogBid(id);
  const { data: productData } = productRepository.hooks.getDetail(id);

  useEffect(() => {
    if (BidData !== null && BidData !== undefined) {
      mutate(setBidData(BidData));
      console.log(bidData, "ini bid");
    }
  }, [BidData]);

  useEffect(() => {
    if (productData !== null && productData !== undefined) {
      setAuctionDetail(productData.data);
      console.log(auctionDetail, "ini auction");
      setStartDate(
        new Date(productData?.data?.start_date).toLocaleDateString("id-ID", {
          timeZone: "Asia/Jakarta",
          hour: "2-digit",
          minute: "2-digit",
        })
      );
      setEndDate(
        new Date(productData?.data?.end_date).toLocaleDateString("id-ID", {
          timeZone: "Asia/Jakarta",
          hour: "2-digit",
          minute: "2-digit",
        })
      );
    }
  }, [productData]);

  return (
    <div className="mt-10 bg-white rounded-md flex p-6 gap-x-10">
      <div className="w-[600px] flex flex-col">
        <div className="w-[600px]  bg-secondary rounded-lg overflow-hidden mb-4 relative">
          <div className="absolute bottom-4 right-4 flex gap-x-2">
            <div className="px-6 py-2 rounded-full bg-primary text-white capitalize  drop-shadow-lg text-sm">
              #{auctionDetail?.tag}
            </div>
          </div>
          <img
            src={`http://localhost:3222/product/get-product-image/${auctionDetail?.picture}`}
            className="object-none w-full object-center max-h-[500px]"
          />
        </div>
        <div className="">
          <p className="font-bold text-xl">{auctionDetail?.name}</p>
          <p className="text-xs leading-relaxed mt-2 text-color-secondary">
            {auctionDetail?.description}
          </p>
        </div>

        <p className="mt-6 mb-2 text-primary font-medium">Price</p>
        <div className="flex justify-between gap-x-6 rounded text-sm bg-secondary p-4">
          <div className="">
            <p className="font-medium mb-2">Open bid</p>
            {toRupiah(auctionDetail?.open_bid)}
          </div>
          <div className="">
            <p className="font-medium mb-2">Auction raise</p>
            {toRupiah(auctionDetail?.auction_raise)}
          </div>
          <div className="">
            <p className="font-medium mb-2">Buy it now</p>
            {toRupiah(auctionDetail?.buy_price)}
          </div>
        </div>
        <p className="mt-6 mb-2 text-primary font-medium">Date</p>
        <div className="flex gap-x-6 rounded text-sm bg-secondary p-4">
          <div className="mr-2">
            <p className="font-medium mb-2">Start date</p>
            {startDate}
          </div>
          <div className="">
            <p className=" font-medium mb-2">End date</p>
            {endDate}
          </div>
        </div>
      </div>
      <div className="w-full">
        <p className="text-md font-semibold text-primary mb-5">Bid history</p>
        <table className="w-full">
          <thead>
            <tr className="bg-secondary font-medium">
              <td className="p-4">Bidder</td>
              <td className="p-4">Bid Time</td>
              <td className="p-4">Bid</td>
            </tr>
          </thead>
          <tbody>
            {bidData.map((v) => {
              return (
                <tr key={v?.id}>
                  <td className="p-4">{v?.user.name}</td>
                  <td className="p-4">{v?.bid_day}</td>
                  <td className="p-4">{toRupiah(v?.price_bid)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

DetailAuction.getLayout = (page) => {
  return <Dashboard title="Auction detail">{page}</Dashboard>;
};
