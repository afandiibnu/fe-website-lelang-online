import { SearchOutlined } from "@ant-design/icons";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import SearchAdmin from "../../../components/SearchAdmin";
import { toRupiah } from "../../../helper/toRupiah";

import Dashboard from "../../../layouts/Dashboard";
import { productRepository } from "../../../repository/product";

const Auction = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);

  const { data: product } = productRepository.hooks.getAllProductApprove({
    page,
    limit,
    search,
  });

  const nextHandler = (e) => {
    e.preventDefault();

    if (data.length == 0) {
      setPage(page + 0);
    } else if (data.length % limit <= 1) {
      setPage(page + 1);
    }
  };

  const previousHandler = (e) => {
    e.preventDefault();

    if (page == 1) {
      setPage(page - 0);
    } else if (page > 1) {
      setPage(page - 1);
    }
  };

  useEffect(() => {
    if (product !== null && product !== undefined) {
      const tempData = product?.items?.map((v) => {
        return {
          id: v.id,
          product: v.name,
          status: v.status,
          picture: v.picture,
          status: v.status,
          auction_raise: v.auction_raise,
          open_bid: v.open_bid,
          buy_price: v.buy_price,
          buy_it_now: v.buy_price,
          start_date: v.start_date,
          end_date: v.end_date,
          buyed: v.buyed,
        };
      });
      console.log(product);
      setData(tempData);
      setLoading(false);
    }
  }, [product]);

  return (
    <div className="w-full py-8">
      {loading ? (
        <div>loading...</div>
      ) : (
        <div className="bg-white p-10">
          <div className=" pb-3  ">
            <div className="relative w-full">
              <label className="">
                <span className="absolute inset-y-0 left-4 flex items-center text-color-secondary text-md">
                  <SearchOutlined></SearchOutlined>
                </span>
                <input
                  className="placeholder:text-color-secondary h-12 block rounded py-2 pl-10 pr-3 focus:outline-none text-sm  w-full focus:ring-2 focus:shadow-md focus:shadow-primary/20 transition duration-100 bg-secondary"
                  placeholder="Search product Name....."
                  type="text"
                  name="search"
                  autoComplete="off"
                  onChange={(e) => setSearch(e.target.value)}
                />
              </label>
            </div>
          </div>

          <div>
            <div className="bg-secondary to-transparent w-full h-[1px]"></div>
            <table className="w-full text-sm table-auto mt-6">
              <thead className="">
                <tr className="bg-secondary">
                  <td className="font-medium px-4 py-4" colSpan={2}>
                    Product
                  </td>
                  <td className="font-medium px-4 py-4">Open Bid</td>
                  <td className="font-medium px-4 py-4">Auction Raise</td>
                  <td className="font-medium px-4 py-4">Buy It Now</td>
                  <td className="font-medium px-4 py-4">Status</td>

                  <td className="font-medium px-4 py-4">Actions</td>
                </tr>
              </thead>
              <tbody>
                {data?.map((v) => {
                  let status;
                  let text;

                  const now = new Date();
                  const start_date = new Date(v.start_date);
                  const end_date = new Date(v.end_date);

                  if (now >= start_date && now <= end_date) {
                    status = "ongoing";
                    text = "text-green-500";
                  }
                  if (now <= start_date) {
                    status = "upcoming";
                    text = "text-yellow-500";
                  }
                  if (now >= end_date) {
                    status = "Finished";
                    text = "text-red-500";
                  }
                  if (v.buyed == true) {
                    status = "Sold out";
                    text = "text-red-500 ";
                  }
                  return (
                    <tr key={v.id}>
                      <td className="py-6">
                        <div className="border rounded-full w-12 h-12 overflow-hidden cursor-pointer">
                          <Link href={`/dashboard/payment/${v.id}`}>
                            <img
                              src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                            />
                          </Link>
                        </div>
                      </td>

                      <td className="pr-4 py-6">{v.product}</td>
                      <td className="px-4 py-6">{toRupiah(v.open_bid)}</td>
                      <td className="px-4 py-6">{toRupiah(v.auction_raise)}</td>
                      <td className="px-4 py-6">{toRupiah(v.buy_it_now)}</td>
                      <td className="px-4 py-6">
                        <span className={text}>
                          {v.buyed == true ? status : status}
                        </span>
                      </td>

                      <td className="px-4 py-4">
                        <div className="flex">
                          <div>
                            <Link href={`/dashboard/auction/${v.id}`}>
                              <button
                                className="text-sm text-center bg-secondary-purple px-3 py-1.5 rounded-full text-primary font-medium hover:shadow-md hover:shadow-primary/30 transiton duration-150"
                                type="submit"
                              >
                                detail
                              </button>
                            </Link>
                          </div>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className="flex justify-end gap-x-2 mt-10">
              <div>
                <button
                  onClick={previousHandler}
                  className="py-1 px-2 bg-secondary-purple text-primary rounded"
                >
                  <FontAwesomeIcon icon={faChevronLeft} />
                </button>
              </div>
              <div className="py-1 px-3 bg-primary text-white rounded">
                {page}
              </div>
              <button
                onClick={nextHandler}
                className="py-1 px-2 bg-secondary-purple text-primary rounded"
              >
                <FontAwesomeIcon icon={faChevronRight} />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Auction;

Auction.getLayout = (page) => {
  return <Dashboard title="Auction">{page}</Dashboard>;
};
