import React from "react";
import Guest from "../layouts/Guest";

const About = () => {
  return (
    <div className="min-h-screen">
      <div className="mt-[100px] text-center flex flex-col items-center">
        <p className="text-3xl font-bold">
          About <span className="text-primary">Aero auction</span>
        </p>
        <p className="w-[800px] mt-6 text-color-secondary">
          Aero auction is a website that connects art enthusiasts and artists. A
          place for artists to sell their works of art so they can be enjoyed by
          art connoisseurs
        </p>
      </div>
    </div>
  );
};

export default About;
About.getLayout = (page) => {
  return <Guest>{page}</Guest>;
};
