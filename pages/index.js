import Guest from "../layouts/Guest";
import Title from "../components/Title";
import Link from "next/link";
import CardOverlay from "../components/Card/CardOverlay";
import CardTitle from "../components/Card/CardTitle";
import CardSubtitle from "../components/Card/CardSubtitle";
import CardWithIcon from "../components/Card/CardWithIcon";
import CardWithButton from "../components/Card/CardWithButton";
import Hero from "../components/Hero";
import SellSection from "../components/SellSection";
import { productRepository } from "/repository/product";
import { useEffect, useState } from "react";
import getTimeLeft from "../helper/getTimeLeft";
import { useRouter } from "next/router";
import { parseJwt } from "../helper/DecodeJwt";

export default function Landingpage() {
  // useAuthenticationPage();
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(3);
  const router = useRouter();
  const [digital, setDigital] = useState([]);
  const [fineAuction, setFineAuction] = useState([]);
  const [limitDigital, setLimitDigital] = useState(8);
  const [limitFine, setLimitFine] = useState(7);

  const { data: product } = productRepository.hooks.getAllProduct({
    page,
    limit,
  });

  const { data: digitals } = productRepository.hooks.getProductDigital({
    page,
    limitDigital,
  });

  const { data: fine } = productRepository.hooks.getProductFine({
    page,
    limitFine,
  });

  useEffect(() => {
    if (product !== null && product !== undefined) {
      setProducts(product.items);
    }
  }, [product]);

  useEffect(() => {
    if (digitals !== null && digitals !== undefined) {
      setDigital(digitals.items);
    }
    if (fine !== null && fine !== undefined) {
      setFineAuction(fine.items);
    }
  }, [digitals, fineAuction]);
  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const decode = parseJwt(token);

  return (
    <div className="scroll-smooth">
      <Hero />
      <div className="py-[100px] px-40">
        <div className="text-center">
          <Title title="Ongoing auction" size="text-2xl" />
        </div>
        {}
        <div className="grid grid-cols-3 gap-8 xl:gap-x-14 mt-10 place-content-center 2xl:px-60">
          {products?.map((v) => {
            const now = new Date();

            const stdate = new Date(v.start_date);
            const enddate = new Date(v.end_date);

            const days = getTimeLeft(enddate, "days");
            const minutes = getTimeLeft(enddate, "minutes");
            const hours = getTimeLeft(enddate, "hours");

            const seller = v?.user?.id;
            const login = decode?.result?.id;

            if (
              now >= stdate &&
              now <= enddate &&
              v.status === "approve" &&
              seller !== login
            ) {
              return (
                <CardOverlay
                  key={v.id}
                  href={`product/categories/${v.category}/${v.id}`}
                >
                  <div>
                    <img
                      src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                      className="absolute top-0 h-full w-full"
                    />
                  </div>

                  <div className="absolute bottom-0 m-6 z-40">
                    <CardTitle title={v.name} color="light-green" />
                    <CardSubtitle
                      subtitle={`${days} D ${hours} H ${minutes} M`}
                      weight="semibold"
                    />
                  </div>
                </CardOverlay>
              );
            }
          })}
        </div>
      </div>

      <div className="py-[100px] bg-secondary px-40">
        <Title title="How to bid?" size="text-2xl" />
        <div className="grid grid-cols-3 gap-x-8 mt-10">
          <CardWithIcon
            background="primary"
            icon="/user.svg"
            title="Sign In"
            titleColor="white"
            subtitle="Login or register to aero auction"
            subtitleColor="white"
          />
          <CardWithIcon
            icon="/search2.svg"
            title="Search"
            subtitle="Search art you want to buy"
            subtitleColor="color-secondary"
          />
          <CardWithIcon
            icon="/bid.svg"
            title="Bid"
            subtitle="Place a bid to item you want to buy"
            subtitleColor="color-secondary"
          />
        </div>
      </div>

      <div className="pt-[100px] px-40 mb-20">
        <div className="flex items-center space-x-6 mb-12">
          <Title title="Digital art auction" size="text-2xl" />
          <Link href="/product/categories/digital/">
            <a className="text-primary">See more</a>
          </Link>
        </div>
        <div className="grid grid-cols-4 gap-x-10 gap-y-16">
          {digital?.map((i) => {
            const now = new Date();

            const stdate = new Date(i.start_date);
            const enddate = new Date(i.end_date);

            const days = getTimeLeft(enddate, "days");
            const minutes = getTimeLeft(enddate, "minutes");
            const hours = getTimeLeft(enddate, "hours");
            const seller = i?.user?.id;
            const login = decode?.result?.id;

            if (
              now >= stdate &&
              now <= enddate &&
              i.status === "approve" &&
              seller !== login
            ) {
              return (
                <CardWithButton
                  key={i.id}
                  size="md"
                  title={i.name}
                  subtitle={`${i.category} Art`}
                  color="black"
                  subtitleColor="color-secondary"
                  weight="bold"
                  href={`product/categories/digital/${i.id}`}
                  text="Detail"
                  image={`http://localhost:3222/product/get-product-image/${i.picture}`}
                  days={days}
                  minutes={minutes}
                  hours={hours}
                />
              );
            }
          })}
        </div>
      </div>

      <div className="px-40 mb-40">
        <div className="flex items-center space-x-6 mb-12">
          <Title title="Fine art auction" size="text-2xl" />
          <Link href="/product/categories/fine/">
            <a className="text-primary">See more</a>
          </Link>
        </div>
        <div className="grid grid-cols-4 gap-x-10 gap-y-16">
          {fineAuction?.map((i) => {
            const now = new Date();

            const stdate = new Date(i.start_date);
            const enddate = new Date(i.end_date);

            const days = getTimeLeft(enddate, "days");
            const minutes = getTimeLeft(enddate, "minutes");
            const hours = getTimeLeft(enddate, "hours");

            const seller = i?.user?.id;
            const login = decode?.result?.id;

            if (
              now >= stdate &&
              now <= enddate &&
              i.status === "approve" &&
              seller !== login
            ) {
              return (
                <CardWithButton
                  key={i.id}
                  size="md"
                  title={i.name}
                  subtitle={`${i.category} Art`}
                  color="black"
                  subtitleColor="color-secondary"
                  weight="bold"
                  href={`product/categories/fine/${i.id}`}
                  text="Detail"
                  image={`http://localhost:3222/product/get-product-image/${i.picture}`}
                  days={days}
                  minutes={minutes}
                  hours={hours}
                />
              );
            }
          })}
        </div>
      </div>

      <div className="py-[100px] px-40 bg-dark-blue">
        <div className="grid grid-cols-2 grid-row-2 gap-x-10 gap-y-52">
          <div>
            <div className="mb-4 text-light-green">
              <Title title="Digital art" />
            </div>

            <Title
              title="Get the best digital arts on Aero auction"
              color="white"
              size="text-5xl"
            />
            <p className="mt-8 text-white">
              Want to have the best digital art collection? Aero auction brings
              you the best digital art
            </p>
          </div>
          <div className="relative">
            <img src="/digital1.png" className="absolute -top-40"></img>
          </div>
          <div className="relative">
            <img src="/fine1.png" className="absolute"></img>
          </div>
          <div>
            <div className="mb-4">
              <Title title="Fine art" color="light-green" />
            </div>

            <Title
              title="Greatest fine art by greatest the artist"
              color="white"
              size="text-5xl"
            />
            <p className="mt-8 text-white">
              Want to have the best fine art collection? Aero auction brings you
              the best fine art
            </p>
          </div>
        </div>
      </div>
      <SellSection />
    </div>
  );
}

Landingpage.getLayout = (page) => {
  return <Guest>{page}</Guest>;
};
