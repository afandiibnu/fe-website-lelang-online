import React from "react";
import { useRouter } from "next/router";
import App from "../../layouts/App";
import Image from "next/image";
import Title from "../../components/Title";
import ButtonPrimary from "../../components/ButtonPrimary";

const ProductDetail = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <div className="grid grid-cols-2 w-full px-40 py-28 2xl:px-96 gap-x-10">
      <div className="w-full">
        <Image src="/legolisa.png" width="540px" height="600px" />
      </div>
      <div className="flex flex-col p-4 w-full">
        <p className="text-sm font-medium text-color-secondary mb-2">
          Digital art
        </p>
        <h1 className="text-5xl font-bold">Product Name</h1>
        <div className="flex mt-4 gap-6 items-center">
          <p className="text-sm">Start from</p>
          <p className="text-sm font-bold bg-light-green py-[10px] px-6 rounded-full">
            Price
          </p>
        </div>
        <p className="text-sm text-color-secondary mt-6">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Gravida
          suspendisse eget enim tincidunt faucibus massa. Vel nunc eget faucibus
          aenean at.
        </p>
        <div className="flex mt-6">
          <p className="text-sm text-primary bg-secondary-purple font-bold rounded-full py-2 px-6">
            Tags
          </p>
        </div>
        <div className="h-full flex flex-col justify-end">
          <Title title="Auction started" size="sm" />
          <div className="flex items-center mt-8 justify-between">
            <div className=" flex text-center gap-x-4">
              <div className="flex flex-col">
                <span className="text-3xl font-bold">00</span>
                <span>DAYS</span>
              </div>
              <div className="flex flex-col">
                <span className="text-3xl font-bold">00</span>
                <span>HOURS</span>
              </div>
              <div className="flex flex-col">
                <span className="text-3xl font-bold">00</span>
                <span>MINUTES</span>
              </div>
            </div>
            <div>
              <ButtonPrimary href="/" text="Buy it now" padding="px-10 py-4" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;

ProductDetail.getLayout = (page) => {
  return <App>{page}</App>;
};
