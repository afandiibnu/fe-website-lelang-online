import Banner from "../../../components/Banner";
import CardWithoutButton from "../../../components/CardWithoutButton";
import Title from "../../../components/Title";
import App from "../../../layouts/App";

const Digital = () => {
  return (
    <div className="relative">
      <Banner>
        <div className="flex justify-center h-full items-center w-full text-center">
          <div className="w-1/3">
            <h1 className="text-5xl font-bold text-white">
              Get the best digital arts on{" "}
              <span className="text-light-green">aero auction</span>
            </h1>
          </div>
        </div>
      </Banner>
      <div className="mx-40 2xl:mx-96 py-28">
        <div>
          <Title title="Auctions" size="text-2xl" />
          <div className="grid grid-cols-4 gap-10 mt-8">
            <CardWithoutButton image={"legolisa.png"} title="zubaedah" />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} title="zubaedah" />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
          </div>
        </div>
      </div>
      <div className="mx-40 2xl:mx-96 py-28">
        <div>
          <Title title="Upcoming auctions" size="text-2xl" />
          <div className="grid grid-cols-4 gap-10 mt-8">
            <CardWithoutButton image={"legolisa.png"} title="zubaedah" />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} title="zubaedah" />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
            <CardWithoutButton image={"legolisa.png"} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Digital;

Digital.getLayout = (page) => {
  return <App title="Digital art">{page}</App>;
};
