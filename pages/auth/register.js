import Link from "next/link";
import Auth from "../../layouts/Auth";
import Button from "../../components/Auth/Button";
import Input from "../../components/Auth/Input";
import Label from "../../components/Auth/Label";
import { useState } from "react";
import { authRepository } from "../../repository/auth";
import { useRouter } from "next/router";
import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export default function Register() {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [confirm, setConfirm] = useState("");
  const [error, setError] = useState("");
  const MySwal = withReactContent(Swal);

  const router = useRouter();

  const handleSubmit = async (e) => {
    try {
      setError("");
      e.preventDefault();
      const data = { name, email, password, confirm };
      await authRepository.manipulateData.createUser(data);
      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Create account success.",
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });

      router.push("/login");
    } catch (e) {
      console.log(e.response);
      const error = e.response.body.message[0];
      MySwal.fire({
        toast: true,
        icon: "error",
        title: `${error}`,
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
    }
  };

  return (
    <div className="max-w-[380px] min-w-[380px]">
      {error && (
        <div className="bg-red-100 text-xs p-4 mb-6 rounded">
          <h1 className="mb-2 font-semibold text-sm text-red-900">
            <FontAwesomeIcon icon={faCircleExclamation} className="mr-2" />
            Error
          </h1>
          <div className="text-red-800">{error}</div>
        </div>
      )}
      <form method="post" className="mb-14" onSubmit={handleSubmit}>
        <div className="mb-6 flex flex-col">
          <Label text={"Email"} target={"email"} />
          <Input
            label={"Email"}
            type={"email"}
            name={"email"}
            id={"email"}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-6 flex flex-col">
          <Label text={"Name"} target={"name"} />
          <Input
            type={"text"}
            name={"name"}
            id={"name"}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="mb-6 flex flex-col">
          <Label text={"Password"} target={"password"} />
          <Input
            type={"password"}
            name={"password"}
            id={"password"}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="mb-6 flex flex-col">
          <Label text={"Confirm password"} target={"confirm"} />
          <Input
            type={"password"}
            name={"confirm"}
            id={"confirm"}
            onChange={(e) => setConfirm(e.target.value)}
          />
        </div>
        <p className="text-xs mb-6">
          By creating an account, you are ready and agree to the{" "}
          <Link href="/terms-condition">
            <span className="font-semibold cursor-pointer">
              terms and conditions
            </span>
          </Link>
        </p>
        <Button action="Sign Up" width="w-full" type="submit" />
      </form>
      <div className="flex justify-center items-center text-sm">
        <p className="mr-2">Have an account ?</p>
        <Link href="/login">
          <span className="mr-2 text-primary font-medium cursor-pointer">
            Sign in
          </span>
        </Link>
      </div>
    </div>
  );
}

Register.getLayout = (page) => <Auth title="Register" children={page} />;
