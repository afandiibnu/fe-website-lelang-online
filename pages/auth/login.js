import Link from "next/link";
import Auth from "../../layouts/Auth";
import Button from "../../components/Auth/Button";
import Input from "../../components/Auth/Input";
import Label from "../../components/Auth/Label";
import axios from "axios";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import { parseJwt } from "../../helper/DecodeJwt";
import { Table } from "antd";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordShown, setPasswordShown] = useState(false);
  const router = useRouter();
  const [error, setError] = useState("");
  const [isLogin, setLogin] = useState(false);
  // const [token, setToken] = useState("");
  const MySwal = withReactContent(Swal);

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();

      const data = {
        email,
        password,
      };

      const endpoint = "http://localhost:3222/auth/login";

      const res = await axios.post(endpoint, data);

      // if (res.data.data !== null && res.data.data !== undefined) {
      localStorage.setItem("token", res.data.request_token);
      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Login success",
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
      router.push("/");
      // }
    } catch (e) {
      console.log(e.response, "response");
      setError(e.response.data.error);
      MySwal.fire({
        toast: true,
        icon: "error",
        title: `${error}`,
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
    }
  };

  let token;

  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");

    // if (token.exp > Date.now()) {
    //   localStorage.removeItem("token");
    //   router.push("/");
    //   console.log(token?.exp);
    // }
  }

  useEffect(() => {
    const decodeJwt = parseJwt(token);
    // console.log(decodeJwt);
    if (decodeJwt) {
      router.push("/");
    }
  }, []);

  const tooglePassword = (e) => {
    e.preventDefault();

    setPasswordShown(!passwordShown);
  };

  return (
    <div className="max-w-[380px] min-w-[380px]">
      <form className="mb-14" onSubmit={handleSubmit}>
        <div className="mb-6 flex flex-col">
          <Label text={"Email"} target="email" />
          <Input
            label={"Email"}
            type={"email"}
            name="email"
            id={"email"}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <div className="mb-4 flex flex-col relative">
          <Label text={"Password"} target="password" />
          <Input
            label={"Password"}
            type={passwordShown ? "text" : "password"}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            name="password"
            id={"password"}
          />
          <button
            onClick={tooglePassword}
            className="absolute right-4 top-[38px] text-gray-400 text-sm"
          >
            {passwordShown ? (
              <FontAwesomeIcon icon={faEye} />
            ) : (
              <FontAwesomeIcon icon={faEyeSlash} />
            )}
          </button>
        </div>
        <Button type="submit" action="Sign In" width="w-full" />
      </form>
      <div className="flex justify-center items-center text-sm">
        <p className="mr-2">Don't have an account ?</p>
        <Link href="/register">
          <span className="mr-2 text-primary font-medium cursor-pointer">
            Sign up
          </span>
        </Link>
      </div>
    </div>
  );
}

Login.getLayout = (page) => <Auth title="Login" children={page} />;
