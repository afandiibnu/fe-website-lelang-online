import React from "react";
import Guest from "../layouts/Guest";

const TermsCondition = () => {
  return (
    <div className="min-h-screen px-64">
      <div className=" mt-[100px] flex items-center flex-col">
        <p className="text-center font-semibold text-3xl">
          Aero auction Terms and Condition
        </p>
        <div className="px-6 py-8 w-[1000px] mt-10">
          <ul className="list-decimal">
            <li className="">
              <p className="font-semibold text-lg">Introduction</p>
              <p className="text-sm mt-2 leading-relaxed">
                By clicking the "Sign Up" button, completing the account
                registration process and using the Aero auction Platform, you
                confirm that you understand and agree to these terms and
                conditions.
              </p>
            </li>
            <li className="mt-4">
              <p className="font-semibold text-lg">Auction Procedure</p>
              <div className="text-sm mt-2 leading-relaxed">
                <ul className="list-disc ml-5">
                  <li>
                    Before participating in the auction, participants must fill
                    in and complete their personal data.
                  </li>
                  {/* <li>
                    Before participating in the auction, participants must pay a
                    security deposit of 50% of the open bid price.
                  </li> */}
                  <li>
                    Bidders are required to maintain the confidentiality of
                    their respective user ID and password. The Internet Auction
                    Operator is not responsible for any consequences of misuse
                    of the Auction Participant's account.
                  </li>
                  <li>
                    Auction participants can buy it now on an item before the
                    auction starts
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      Auction participants cannot bid after the auction is over.
                    </p>
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      The auction winner is determined by the highest price
                      after the auction is over.
                    </p>
                  </li>
                </ul>
              </div>
            </li>
            <li className="mt-4">
              <p className="font-semibold text-lg">Payments</p>
              <div className="text-sm mt-2 leading-relaxed">
                <ul className="list-disc ml-5">
                  <li>
                    Before participating in the auction, participants must pay a
                    security deposit of 50% of the open bid price.
                  </li>
                  <li>
                    After being determined as the winner, the auction
                    participant must pay the remaining payment no later than 7
                    days after the auction is over
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      If the participant loses the auction then the deposit that
                      has been paid in advance will be returned as much as 90%
                    </p>
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      The seller of the auction item will receive payment three
                      day after the auction winner pays the remaining payment
                    </p>
                  </li>
                </ul>
              </div>
            </li>
            <li className="mt-4">
              <p className="font-semibold text-lg">Auction item shipping</p>
              <div className="text-sm mt-2 leading-relaxed">
                <ul className="list-disc ml-5">
                  <li>
                    <p className="leading-relaxed">
                      Aero auction does not handle The cost of shipping goods.
                      the seller must set the shipping cost as well as set the
                      price of the art being sold
                    </p>
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      Fine arts will be shipped no later than 3 days after
                      payment is made
                    </p>
                  </li>
                  <li>
                    <p className="leading-relaxed">
                      Digital art will be sent by the seller via google drive or
                      to the buyer no later than three days after payment is
                      made
                    </p>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default TermsCondition;

TermsCondition.getLayout = (page) => {
  return <Guest>{page}</Guest>;
};
