import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import Tabel from "../../../components/Profile/Tabel";
import Wrapper from "../../../components/Profile/Wrapper";
import { toRupiah } from "../../../helper/toRupiah";
import Layout from "../../../layouts/Profile";
import { productRepository } from "../../../repository/product";

const columns = [
  { Header: "Name", accessor: "name" },
  { Header: "Status", accessor: "status" },
  { Header: "Final price", accessor: "final_price" },
  { Header: "Date", accessor: "date" },
];

const BidAuction = () => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(5);
  const [loading, setLoading] = useState(true);

  const { data: product } = productRepository.hooks.getBidHistory({
    page,
    limit,
  });

  // console.log(data);
  useEffect(() => {
    if (product !== null && product !== undefined) {
      const tempData = product?.items?.map((v) => {
        return {
          id: v.id,
          date: v.bid_day,
          status: v.status,
          name: v.product.name,
          price_bid: v.price_bid,
        };
      });

      setData(tempData);
      setLoading(false);
    }
  }, [product]);

  const nextHandler = (e) => {
    e.preventDefault();

    if ((data.length = 0)) {
      setPage(page + 0);
    } else if (data.length >= 1) {
      setPage(page + 1);
    }
  };

  const previousHandler = (e) => {
    e.preventDefault();

    if (page == 1) {
      setPage(page - 0);
    } else if (page > 1) {
      setPage(page - 1);
    }
  };
  return (
    <div>
      <Wrapper
        title="Bid history"
        subtitle="Here is your Bid history."
        padding="p-6"
      >
        {" "}
        {loading ? (
          <div>loading...</div>
        ) : (
          <table className="w-full text-sm table-auto">
            <thead className="">
              <tr className="bg-secondary">
                <td className="font-medium px-4 py-4">Auction name</td>
                <td className="font-medium px-4 py-4">Status</td>
                <td className="font-medium px-4 py-4">Bid</td>
                <td className="font-medium px-4 py-4">Date</td>
              </tr>
            </thead>
            <tbody>
              {data?.map((v) => {
                return (
                  <tr key={v.id}>
                    <td className="px-4 py-8">{v.name}</td>
                    <td className="px-4 py-8">{v.status}</td>
                    <td className="px-4 py-8">{toRupiah(v.price_bid)}</td>
                    <td className="px-4 py-8">{v.date}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
        <div className="flex justify-end  gap-x-2 mt-10">
          <div>
            <button
              onClick={previousHandler}
              className="py-1 px-2 bg-secondary-purple text-primary rounded"
            >
              <FontAwesomeIcon icon={faChevronLeft} />
            </button>
          </div>
          <div className="py-1 px-3 bg-primary text-white rounded">{page}</div>
          <button
            onClick={nextHandler}
            className="py-1 px-2 bg-secondary-purple text-primary rounded"
          >
            <FontAwesomeIcon icon={faChevronRight} />
          </button>
        </div>
      </Wrapper>
    </div>
  );
};

export default BidAuction;
BidAuction.getLayout = (page) => {
  return <Layout title="Auction history">{page}</Layout>;
};
