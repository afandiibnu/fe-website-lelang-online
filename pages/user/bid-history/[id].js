import React from "react";
import Wrapper from "../../../components/Profile/Wrapper";
import Layout from "../../../layouts/Profile";
import TabelNA from "../../../components/Profile/TabelWithoutAction";

const data = [
  { name: "Ibnu namikaze", bid: "2000000" },
  { name: "Wahyu Nara", bid: "1500000" },
  { name: "Yoga Sarutobi", bid: "1400000" },
  { name: "Arta Senju", bid: "1300000" },
  { name: "Sultan Uchiha", bid: "1200000" },
  { name: "Aziz uzumaki", bid: "1000000" },
];

const columns = [
  { Header: "Bidder", accessor: "name" },
  { Header: "Bids", accessor: "bid" },
];

const HistoryDetail = () => {
  return (
    <div>
      <Wrapper
        title="Name auction"
        subtitle="this is (auction name) bids history"
        padding="p-6"
      >
        <div className="w-full flex mb-10 justify-between items-center">
          <p className="text-xl font-semibold">Winner</p>
          <div className="flex gap-x-4 items-center font-medium text-sm">
            <p>{data[0].name}</p>
            <div className="px-4 py-2 rounded-full bg-light-green font-bold">
              {data[0].bid}
            </div>
          </div>
        </div>
        <TabelNA cols={columns} API={data}></TabelNA>
      </Wrapper>
    </div>
  );
};

export default HistoryDetail;

HistoryDetail.getLayout = (page) => {
  return <Layout>{page}</Layout>;
};
