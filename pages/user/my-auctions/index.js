import React, { useEffect, useState } from "react";
import Layout from "../../../layouts/Profile";
import Wrapper from "../../../components/Profile/Wrapper";
import Tabel from "../../../components/Profile/Tabel";
import { productRepository } from "../../../repository/product";
import ButtonPrimary from "../../../components/ButtonPrimary";
import { mutate } from "swr";

const MyAuction = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { data: product } = productRepository.hooks.getMyAuction();

  mutate(productRepository.url.getMyAuction());
  useEffect(() => {
    if (product !== null && product !== undefined) {
      const tempProduct = product.map((v) => {
        return { name: v.name, status: v.status, id: v.id };
      });

      setData(tempProduct);
      setLoading(false);
    }
  }, [product]);

  const columns = [
    { Header: "Name", accessor: "name" },
    { Header: "Status", accessor: "status" },
  ];

  return (
    <div>
      <Wrapper title="My Auctions" subtitle="Here is your auctions list.">
        {loading ? <div>loading...</div> : <Tabel cols={columns} API={data} />}
      </Wrapper>
    </div>
  );
};

export default MyAuction;

MyAuction.getLayout = (page) => {
  return <Layout title={"My auctions"}>{page}</Layout>;
};
