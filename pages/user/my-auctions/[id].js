import Image from "next/image";
import React, { useEffect, useState } from "react";
import Wrapper from "../../../components/Profile/Wrapper";
import Layout from "../../../layouts/Profile";
import { productRepository } from "../../../repository/product";
import { useRouter } from "next/router";
import { toRupiah } from "../../../helper/toRupiah";
import { bidRepository } from "../../../repository/bid";

const DetailAuction = () => {
  const [product, setProduct] = useState([]);
  const [bid, setBid] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const [convertSTDate, setConvertSTDate] = useState("");
  const [convertEndDate, setConvertEndDate] = useState("");

  const { data: productData } = productRepository.hooks.getDetail(id);
  const { data: bidData } = bidRepository.hooks.getLogBid(id);
  // console.log(productData);

  useEffect(() => {
    if (productData !== null && productData !== undefined) {
      setProduct(productData.data);
      setConvertSTDate(
        new Date(productData?.data?.start_date).toLocaleDateString("id-ID", {
          timeZone: "Asia/Jakarta",
          hour: "2-digit",
          minute: "2-digit",
        })
      );
      setConvertEndDate(
        new Date(productData?.data?.end_date).toLocaleDateString("id-ID", {
          timeZone: "Asia/Jakarta",
          hour: "2-digit",
          minute: "2-digit",
        })
      );
    }

    if (bidData !== null && bidData !== undefined) {
      setBid(bidData);
    }
  }, [productData, bidData]);

  return (
    <>
      <div className="rounded-md w-12/12 relative flex gap-x-10">
        <div className="overflow-hidden rounded-md">
          <img
            src={`http://localhost:3222/product/get-product-image/${productData?.data?.picture}`}
            className="h-[500px] object-cover"
          />
        </div>

        <div className="w-8/12">
          <p className="text-sm text-color-secondary font-medium mb-2 capitalize">
            {product.category}
          </p>
          <h1 className="text-4xl font-bold">{product.name}</h1>
          <p className="text-sm text-color-secondary mt-6">
            {product.description}
          </p>

          <div className="mt-6 flex gap-x-2 items-center">
            <div className="bg-light-green font-bold px-6 py-2 rounded-full">
              #{product.tag}
            </div>
          </div>

          <div className="mt-6 flex gap-x-6 divide-x">
            <div className="flex gap-x-2 font-semibold">
              <p>Open bid</p>
              <p className="text-primary font-bold">
                {toRupiah(product.open_bid)}
              </p>
            </div>

            <div className="flex gap-x-2 font-semibold pl-6">
              <p>Buy it now </p>
              <p className="text-primary font-bold">
                {toRupiah(product.buy_price)}
              </p>
            </div>

            <div className="flex gap-x-2 font-semibold pl-6">
              <p>Auction Rise</p>
              <p className="text-primary font-bold">
                {toRupiah(product.auction_raise)}
              </p>
            </div>
          </div>

          <div className="flex mt-20 gap-x-10">
            <div className="flex flex-col bg-secondary gap-x-4 p-8 rounded">
              <p className="mb-4">Auction started :</p>

              <div className=" flex text-center gap-x-6 items-center text-3xl font-bold">
                {convertSTDate}
              </div>
            </div>
            <div className="flex flex-col bg-secondary gap-x-4 p-8 rounded">
              <p className="mb-4">Auction ended :</p>
              <div className=" flex text-center gap-x-6 items-center text-3xl font-bold">
                {convertEndDate}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-10">
        <h1 className="text-lg font-medium text-primary">Bid history</h1>
        <table className="w-full mt-6">
          <thead>
            <tr className="bg-secondary">
              <td className="px-6 py-3">Bidder</td>
              <td className="px-6 py-3">Bid date</td>
              <td className="px-6 py-3">Bid</td>
            </tr>
          </thead>
          <tbody>
            {bid.map((i) => {
              return (
                <tr>
                  <td className="px-6 py-3">{i?.user?.name}</td>
                  <td className="px-6 py-3">
                    {new Date(i?.bid_day).toLocaleDateString("id-ID", {
                      timeZone: "Asia/Jakarta",
                      hour: "2-digit",
                      minute: "2-digit",
                    })}
                  </td>
                  <td className="px-6 py-3">{toRupiah(i?.price_bid)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default DetailAuction;

DetailAuction.getLayout = (page) => {
  return <Layout title={"My auctions detail"}>{page}</Layout>;
};
