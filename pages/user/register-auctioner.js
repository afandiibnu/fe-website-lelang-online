import React, { useState } from "react";
import Wrapper from "../../components/Profile/Wrapper";
import Layout from "../../layouts/Profile";
import Input from "../../components/Profile/Input";
import Button from "../../components/Auth/Button";
import { userRepository } from "../../repository/user";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { mutate } from "swr";
import { useRouter } from "next/router";

const RegisterAuctioner = () => {
  const [picture, setPicture] = useState("/pipic.svg");
  const [pictureName, setPictureName] = useState("");
  const [imageValue, setImageValue] = useState();
  const MySwal = withReactContent(Swal);

  const router = useRouter();

  const handleImage = async (e) => {
    e.preventDefault();

    try {
      const file = e.target.files[0];
      const result = await userRepository.manipulateData.uploadKTP(file);

      setPicture(URL.createObjectURL(file));
      setPictureName(e.target.files[0].name);
      setImageValue(JSON.parse(result.text).data);

      MySwal.fire({
        title: <p className="text-lg">Upload image success</p>,
        icon: "success",
      });

      console.log(result);
    } catch (e) {
      console.log(e);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await userRepository.manipulateData.updateKTP({
        ktp: imageValue,
      });

      MySwal.fire({
        toast: true,
        icon: "success",
        title: "You become an auctioner.",
        text: "please login again after you become an auctioner",
        position: "top",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
      });

      localStorage.removeItem("token");

      mutate(await router.push("/login"));
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <Wrapper
      title="Become an auctioner"
      subtitle="Register yourself and become auctioner to sell your art"
      padding="p-6"
    >
      <form method="post" onSubmit={handleSubmit}>
        <div>
          <label className="block text-sm font-medium">Upload ID card</label>
          <div className="mt-2 flex justify-center px-6 py-6 border border-gray-300 rounded-md">
            <div className="space-y-1 items-center">
              <div className="mb-4 flex flex-col items-center">
                <img src={picture} className="w-56 mb-2" />
                <p className="text-xs text-center text-color-secondary">
                  {pictureName}
                </p>
              </div>

              <div className="flex text-sm text-gray-600 justify-center ">
                <label
                  htmlFor="file-upload"
                  className="relative cursor-pointer rounded-md font-medium text-primary hover:text-primary-hover focus-within:outline-none focus-within:ring-2 p-2 focus-within:ring-primary/50"
                >
                  <span>Add Image</span>
                  <input
                    id="file-upload"
                    name="picture"
                    accept="application/jpg, .jpeg, .png"
                    type="file"
                    className="sr-only"
                    onChange={handleImage}
                  />
                </label>
              </div>

              {/* <p className="text-xs text-gray-500">
                  PNG, JPG, GIF up to 10MB
                </p> */}
            </div>
          </div>
        </div>
        <div className="mt-16">
          <Button action="Submit" width="w-40" />
        </div>
      </form>
    </Wrapper>
  );
};

export default RegisterAuctioner;

RegisterAuctioner.getLayout = (page) => {
  return <Layout title="Register as auctioner">{page}</Layout>;
};
