import React, { useEffect, useState } from "react";
import Layout from "../../layouts/Profile";
import Wrapper from "../../components/Profile/Wrapper";
import Input from "../../components/Profile/Input";
import InputImage from "../../components/Profile/InputImage";
import Button from "../../components/Auth/Button";
import { accountRepository } from "../../repository/account";
import { mutate } from "swr";
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { userRepository } from "../../repository/user";

const Account = () => {
  const [account, setAccount] = useState([]);
  const [email, setEmail] = useState(account);
  const [success, setSuccess] = useState("");
  const [user, setUser] = useState([]);
  const [imageValue, setImageValue] = useState(user?.picture);

  const { data: accountData } = accountRepository.hooks.getAccountDetail();
  const { data: userData } = userRepository.hooks.getUserDetail();

  useEffect(() => {
    if (accountData !== null && accountData !== undefined) {
      setEmail(accountData.email);
    }
  }, [accountData]);

  const router = useRouter();

  const imageHandler = async (e) => {
    e.preventDefault();

    try {
      const file = e.target.files[0];
      const result = await userRepository.manipulateData.uploadUserImage(file);
      setImageValue(JSON.parse(result.text).data);
    } catch (e) {
      console.log(e);
    }
  };

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();

      const data = { email };
      await accountRepository.manipulateData.updateAccount(data);
      await userRepository.manipulateData.updatePicture({
        picture: imageValue,
      });

      // setSuccess("Edit data success!");
      const MySwal = withReactContent(Swal);
      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Edit Account success!",
        position: "top",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
      });
      mutate(userRepository.url.userDetail(), router.push("/user/account"));
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData.data);
    }
  }, [userData]);

  return (
    <div className="min-h-screen">
      <Wrapper
        title="Account"
        subtitle="You can edit account information here."
        padding="p-6"
      >
        <form method="post" onSubmit={handleSubmit}>
          <Input
            label={"Email"}
            type={"email"}
            name="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <div>
            <label htmlFor="picture" className="text-sm font-medium">
              Edit image
            </label>
            <div className="flex items-center space-x-6 mt-2">
              <label className="block">
                <span className="sr-only"></span>
                <input
                  type="file"
                  className="block w-full text-sm text-color-secondary file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-reguler file:text-primary file:bg-secondary-purple"
                  name="picture"
                  onChange={imageHandler}
                />
              </label>
            </div>
          </div>

          <div className="mt-16">
            <Button action="Save changes" width="w-40" type="submit" />
          </div>
        </form>
      </Wrapper>
    </div>
  );
};

export default Account;

Account.getLayout = (page) => {
  return <Layout title="Account">{page}</Layout>;
};
