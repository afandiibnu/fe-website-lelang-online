import React from "react";
import Wrapper from "../../components/Profile/Wrapper";
import Layout from "../../layouts/Profile";
import Input from "../../components/Profile/Input";
import Button from "../../components/Auth/Button";
import { productRepository } from "../../repository/product";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { useRouter } from "next/router";
import { parseJwt } from "../../helper/DecodeJwt";
import useAuthenticationPage from "../../helper/authenticationPage";

const Sell = () => {
  useAuthenticationPage();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("digital");
  const [tag, setTag] = useState("");
  const [start_date, setStartDate] = useState("");
  const [end_date, setEndDate] = useState("");
  const [open_bid, setOpenBid] = useState();
  const [buy_price, setBuyPrice] = useState();
  const [auction_raise, setAuctionRaise] = useState();
  const [picture, setPicture] = useState("/pipic.svg");
  const [pictureName, setPictureName] = useState("");
  const [pictureCopyRight, setPictureCopyright] = useState("/pipic.svg");
  const [pictureNameCopyRight, setPictureNameCopyRIght] = useState("");
  const [imageValue, setImageValue] = useState("");
  const [imageCopyrightValue, setImageCopyrightValue] = useState("");

  const MySwal = withReactContent(Swal);
  const router = useRouter();

  let token;

  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }

  useEffect(() => {
    const decodeJwt = parseJwt(token);

    if (decodeJwt.result.account.role.description !== "Auctioner") {
      router.push("/user/register-auctioner");
    }
  });

  const handleImage1 = async (e) => {
    // const [file, setFile] = useState([]);
    e.preventDefault();

    try {
      const file = e.target.files[0];
      const result = await productRepository.manipulateData.uploadProductImage(
        file
      );

      setPicture(URL.createObjectURL(file));
      setPictureName(e.target.files[0].name);
      setImageValue(JSON.parse(result.text).data);

      MySwal.fire({
        title: <p className="text-lg">Upload image success</p>,
        icon: "success",
      });
    } catch (e) {
      console.log(e, "ini error");
    }
  };
  const handleImage2 = async (e) => {
    e.preventDefault();

    try {
      const file = e.target.files[0];
      const result =
        await productRepository.manipulateData.uploadProductImageCopyright(
          file
        );

      setPictureCopyright(URL.createObjectURL(file));
      setPictureNameCopyRIght(e.target.files[0].name);
      setImageCopyrightValue(JSON.parse(result.text).data);

      MySwal.fire({
        title: <p className="text-lg">Upload image success</p>,
        icon: "success",
      });
    } catch (e) {
      console.log(e.response, "ini error");
    }
  };

  const submitHandler = async (e) => {
    e.preventDefault();

    try {
      const data = {
        name,
        description,
        category,
        tag,
        start_date,
        end_date,
        open_bid,
        buy_price,
        auction_raise,
        picture: imageValue,
        copyright: imageCopyrightValue,
      };

      await productRepository.manipulateData.createProduct(data);

      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Sell art success!",
        text: "Wait until admin confirm your art",
        position: "bottom-right",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
      });

      router.push("/user/my-auctions");
    } catch (e) {
      console.log(e.response.body.message[0]);
    }
  };

  return (
    <div>
      <Wrapper
        title="Sell your art"
        subtitle="Show your arts to the world"
        padding="p-6"
      >
        <form action="post" onSubmit={submitHandler}>
          <Input
            label="Art name"
            type="text"
            name="name"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
          <Input
            label="Description"
            type="text"
            name="description"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          />
          <div className="flex w-full gap-x-4">
            <div className="w-full">
              <label htmlFor="type" className="text-sm font-medium">
                Art category
              </label>
              <select
                id="type"
                name="type"
                autoComplete="Art type"
                className="block w-full h-10 px-4 border border-gray-300 bg-white rounded shadow-sm focus:outline-none focus:ring-1 ring-primary/30 mt-2"
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value={"digital"} selected>
                  Digital art
                </option>
                <option value={"fine"}>Fine art</option>
              </select>
            </div>
            <div className="w-full">
              <Input
                label="Tags"
                type="text"
                name="tags"
                onChange={(e) => setTag(e.target.value)}
                value={tag}
              />
            </div>
          </div>
          <div className="flex w-full gap-x-4">
            <div className="w-full">
              <Input
                label="Open bid"
                type="number"
                name="open_bid"
                onChange={(e) => setOpenBid(e.target.value)}
                value={open_bid}
              />
            </div>
            <div className="w-full">
              <Input
                label="Buy it now price"
                type="number"
                name="bin_price"
                onChange={(e) => setBuyPrice(e.target.value)}
                value={buy_price}
              />
            </div>
            <div className="w-full">
              <Input
                label="Raise price"
                type="number"
                name="raise_price"
                onChange={(e) => setAuctionRaise(e.target.value)}
                value={auction_raise}
              />
            </div>
          </div>
          <div className="flex w-full gap-x-4">
            <div className="w-full">
              <Input
                label="Start date"
                type="datetime-local"
                name="open_bid"
                onChange={(e) => setStartDate(e.target.value)}
                value={start_date}
              />
            </div>
            <div className="w-full">
              <Input
                label="End date"
                type="datetime-local"
                name="bin_price"
                onChange={(e) => setEndDate(e.target.value)}
                value={end_date}
              />
            </div>
          </div>
          <div>
            <label className="block text-sm ">
              <p className="font-medium">Product Photo</p>
              <p className="text-xs mt-2 mb-4 text-color-secondary">
                if your art type is digital you must upload art photo with the
                watermark
              </p>
            </label>
            <div className="mt-2 flex justify-center px-6 py-6 border border-gray-300 rounded-md">
              <div className="space-y-1 items-center">
                <div className="mb-4">
                  <img src={picture} className="w-24 mb-2" />
                  <p className="text-xs text-center text-color-secondary">
                    {pictureName}
                  </p>
                </div>

                <div className="flex text-sm text-gray-600 justify-center ">
                  <label
                    htmlFor="file-upload"
                    className="relative cursor-pointer rounded-md font-medium text-primary hover:text-primary-hover focus-within:outline-none focus-within:ring-2 p-2 focus-within:ring-primary/50"
                  >
                    <span>Add Image</span>
                    <input
                      id="file-upload"
                      name="file-upload"
                      accept="application/jpg, .jpeg, .png"
                      type="file"
                      className="sr-only"
                      onChange={handleImage1}
                    />
                  </label>
                </div>

                {/* <p className="text-xs text-gray-500">
                  PNG, JPG, GIF up to 10MB
                </p> */}
              </div>
            </div>
          </div>
          <div className="mt-6">
            <label className="block text-sm ">
              <p className="font-medium">Copyright Certificate Photo</p>
              <p className="text-xs mt-2 mb-4 text-color-secondary">
                Please upload certificate if your art type is digital
              </p>
            </label>
            <div className="mt-2 flex justify-center items-center px-6 py-6 border border-gray-300 rounded-md">
              <div className="space-y-1 items-center">
                <div className="mb-4 flex flex-col items-center">
                  <img src={pictureCopyRight} className="w-24 mb-2" />
                  <p className="text-xs text-center text-color-secondary">
                    {pictureNameCopyRight}
                  </p>
                </div>

                <div className="flex text-sm text-gray-600 justify-center ">
                  <label
                    htmlFor="file-upload-copyright"
                    className="relative cursor-pointer rounded-md font-medium text-primary hover:text-primary-hover focus-within:outline-none focus-within:ring-2 p-2 focus-within:ring-primary/50"
                  >
                    <span>Add Image</span>
                    <input
                      id="file-upload-copyright"
                      name="file-upload"
                      accept="application/jpg, .jpeg, .png,.jpg"
                      type="file"
                      className="sr-only"
                      onChange={handleImage2}
                    />
                  </label>
                </div>

                {/* <p className="text-xs text-gray-500">
                  PNG, JPG, GIF up to 10MB
                </p> */}
              </div>
            </div>
          </div>
          <div className="mt-16">
            <Button action="Sell art" width="w-40" />
          </div>
        </form>
      </Wrapper>
    </div>
  );
};

export default Sell;

Sell.getLayout = (page) => {
  return <Layout title={"Sell art"}>{page}</Layout>;
};
