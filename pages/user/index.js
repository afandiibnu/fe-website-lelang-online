import Layout from "../../layouts/Profile";
import Input from "../../components/Profile/Input";
import Button from "../../components/Auth/Button";
import React, { useState } from "react";
import Wrapper from "../../components/Profile/Wrapper";
import { userRepository } from "../../repository/user";
import { useEffect } from "react";
import { Breadcrumb } from "antd";
import { mutate } from "swr";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { faX } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const Profile = () => {
  const [success, setSuccess] = useState("");
  const [isShow, setIsShow] = useState(false);
  const [user, setUser] = useState(null);
  const [name, setName] = useState(user?.name);
  const [nik, setNik] = useState(user?.nik);
  const [phone_number, setPhone] = useState(user?.phone_number);
  const [bank_account_number, setBan] = useState(user?.bank_account_number); //bank account number
  const [addres, setAddres] = useState(user?.addres);
  const [postal_code, setPostalCode] = useState(user?.postal_code);

  const { data: userData } = userRepository.hooks.getUserDetail();

  const router = useRouter();
  const MySwal = withReactContent(Swal);

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      const data = {
        name,
        user,
        nik,
        phone_number,
        bank_account_number,
        addres,
        postal_code,
      };

      await userRepository.manipulateData.updateUser(data);

      setSuccess("Edit data success!");

      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Edit Profile success!",
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
      mutate(userRepository.url.userDetail(), router.push("/user/"));
    } catch (e) {
      console.log(e.response);
      MySwal.fire({
        toast: true,
        icon: "error",
        title: (
          <p className="text-sm font-regular">{e.response.body.message[0]}</p>
        ),
        position: "top",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
      });
    }
  };

  const handleIsShow = () => {
    setIsShow(!isShow);
  };

  useEffect(() => {
    if (userData !== null && userData !== undefined) {
      setUser(userData.data);
    }
  }, [userData]);

  return (
    <div>
      <Wrapper
        title="Profile"
        subtitle="Here is your profile information. You can edit here"
        padding="p-6"
      >
        {/* {error && (
          <div className="bg-red-100 text-xs p-4 mb-6 rounded">
            <h1 className="mb-2 font-semibold text-sm text-red-900">
              <FontAwesomeIcon icon={faCircleExclamation} className="mr-2" />
              Error
            </h1>
            <div className="text-red-800">{error}</div>
          </div>
        )} */}
        <form method="post" onSubmit={handleSubmit}>
          <Input
            label="Name"
            type="text"
            name="name"
            onChange={(e) => {
              setName(e.target.value);
            }}
            value={user?.name}
          />
          <Input
            label="NIK"
            type="number"
            name="nik"
            value={user?.nik}
            onChange={(e) => {
              setNik(e.target.value);
            }}
          />
          <Input
            label="Phone number"
            type="number"
            name="phone_number"
            value={user?.phone_number}
            onChange={(e) => {
              setPhone(e.target.value);
            }}
          />
          <Input
            label="Bank account number"
            type="number"
            name="bank_account_number"
            value={user?.bank_account_number}
            onChange={(e) => {
              setBan(e.target.value);
            }}
          />
          <Input
            label="Address"
            type="text"
            name="addres"
            value={user?.addres}
            onChange={(e) => {
              setAddres(e.target.value);
            }}
          />
          <Input
            label="Postal code"
            type="number"
            name="postal_code"
            value={user?.postal_code}
            onChange={(e) => {
              setPostalCode(e.target.value);
            }}
          />
          <div className="mt-16">
            <Button action="Save changes" width="w-40" type="submit" />
          </div>
        </form>
      </Wrapper>
    </div>
  );
};

export default Profile;

Profile.getLayout = (page) => {
  return <Layout title="Profile">{page}</Layout>;
};
