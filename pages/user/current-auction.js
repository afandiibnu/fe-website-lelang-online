import React, { useMemo } from "react";
import Tabel from "../../components/Profile/Tabel";
import Wrapper from "../../components/Profile/Wrapper";
import Layout from "../../layouts/Profile";

const data = [
  { id: 1, name: "deden auction", highest_bid: 100000 },
  { id: 2, name: "satria auction", highest_bid: 100000 },
  { id: 3, name: "indah auction", highest_bid: 100000 },
  { id: 4, name: "indah auction", highest_bid: 100000 },
];

const columns = [
  { Header: "Name", accessor: "name" },
  { Header: "Highest bid", accessor: "highest_bid" },
];

const CurrentAuction = () => {
  return (
    <div>
      <Wrapper
        title="Current auctions"
        subtitle="Here is your current auction that you followed"
      >
        <Tabel cols={columns} API={data} />
      </Wrapper>
    </div>
  );
};

export default CurrentAuction;

CurrentAuction.getLayout = (page) => {
  return <Layout title="Current auctions">{page}</Layout>;
};
