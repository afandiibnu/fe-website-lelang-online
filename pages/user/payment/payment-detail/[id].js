import React, { useEffect, useState } from "react";
import Layout from "../../../../layouts/Profile";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { paymentRepository } from "../../../../repository/payment";
import { toRupiah } from "../../../../helper/toRupiah";

const PaymentDetail = () => {
  const router = useRouter();
  const { id } = router.query;

  const [payment, setPayment] = useState([]);
  const [purchaseDate, setPurchaseDate] = useState("");

  const { data: paymentData } = paymentRepository.hooks.getPaymentDetail(id);

  useEffect(() => {
    if (paymentData !== null && paymentData !== undefined) {
      setPayment(paymentData?.data[0]);
      setPurchaseDate(
        new Date(paymentData?.data[0]?.purchase_date).toLocaleDateString(
          "id-ID",
          {
            timeZone: "Asia/Jakarta",
            hour: "2-digit",
            minute: "2-digit",
          }
        )
      );
    }
  }, [paymentData]);

  return (
    <>
      <div className="border border-[#e8e8e8] rounded-md px-14 py-6 min-h-screen">
        <div className="flex justify-between items-center">
          <div>
            <Image src={"/logoLight.svg"} width={"80px"} height={"40px"} />
          </div>
          <div className="uppercase text-base font-medium">{payment?.id}</div>
        </div>
        <div className="my-14 flex justify-between">
          <div className="grid grid-cols-2 gap-y-4 gap-x-6 text-sm w-4/12">
            <div className="font-semibold capitalize">buyers name</div>
            <div>{payment?.user?.name}</div>
            <div className="font-semibold capitalize">Phone number</div>
            <div>{payment?.user?.phone_number}</div>
            <div className="font-semibold capitalize">address</div>
            <div>{payment?.user?.addres}</div>
            <div className="font-semibold capitalize">payment date</div>
            <div>{purchaseDate}</div>
          </div>
          <div className="grid grid-cols-2 gap-y-4 gap-x-6 text-sm">
            <div className="font-semibold capitalize">Seller</div>
            <div>{payment?.product?.user?.name}</div>
          </div>
        </div>
        <div className="mt-16">
          <table className="w-full">
            <thead>
              <tr className="text-start font-semibold bg-secondary ">
                <td className="w-10 py-6 px-4">No.</td>
                <td>Product</td>
                <td className="w-60">Unit Price</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="w-10 py-6 px-4">1</td>
                <td>{payment?.product?.name}</td>
                <td>{toRupiah(payment?.fixed_price)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex gap-x-6 font-bold justify-end self-end mt-20">
          <p>Total</p>
          <p>{toRupiah(payment?.fixed_price)}</p>
        </div>
      </div>
      <Link href={`http://localhost:3222/report/download/pdf/${id}`}>
        <button className="mt-6 px-6 py-4 rounded bg-primary text-sm font-medium text-white">
          Download as PDF
        </button>
      </Link>
    </>
  );
};

export default PaymentDetail;

PaymentDetail.getLayout = (page) => {
  return <Layout title="Payment detail">{page}</Layout>;
};
