import React, { useEffect, useState } from "react";
import Layout from "../../../../layouts/Profile";
import Wrapper from "../../../../components/Profile/Wrapper";
import Button from "../../../../components/Auth/Button";
import { productRepository } from "../../../../repository/product";
import { useRouter } from "next/router";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import { paymentRepository } from "../../../../repository/payment";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { toRupiah } from "../../../../helper/toRupiah";

const PaymentReceiptUpload = () => {
  useAuthenticationPage();
  const [payment, setPayment] = useState([]);
  const [insurance, setInsurance] = useState([]);
  const [imageValue, setImageValue] = useState("");
  const [picture, setPicture] = useState("/pipic.svg");
  const [pictureName, setPictureName] = useState("");
  const router = useRouter();
  const { id } = router.query;
  const MySwal = withReactContent(Swal);
  const [productId, setProductId] = useState(" ");

  const { data: paymentData } = paymentRepository.hooks.getPaymentDetail(id);
  const { data: insuranceData } =
    paymentRepository.hooks.getInsuranceByProductData(productId);

  useEffect(() => {
    if (paymentData !== null && paymentData !== undefined) {
      setPayment(paymentData.data);
    }
  }, [paymentData]);

  useEffect(() => {
    if (insuranceData !== null && insuranceData !== undefined) {
      setInsurance(insuranceData.data);
      setProductId(payment?.product?.id);
    }
  }, [insuranceData]);

  const handleImage = async (e) => {
    e.preventDefault();

    try {
      const file = e.target.files[0];

      const result =
        await paymentRepository.manipulateData.uploadPaymentPicture(file);

      setPicture(URL.createObjectURL(file));
      setPictureName(e.target.files[0].name);
      setImageValue(JSON.parse(result.text).data);
      MySwal.fire({
        title: <p className="text-lg">Upload image success</p>,
        icon: "success",
      });
    } catch (e) {
      console.log(e);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const data = { proof_of_payment: imageValue };

      await paymentRepository.manipulateData.updatePayment(id, data);
      MySwal.fire({
        toast: true,
        icon: "success",
        title: "Sell art success!",
        text: "Wait until admin confirm your payment",
        position: "bottom-right",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
      });
      router.push("/user/payment/payment-list/");
    } catch (e) {
      console.log(e.response);
    }
  };

  const insurancePay = insurance[0]?.price;

  const amountDue = payment[0]?.fixed_price - insurancePay;

  // console.log(openbid);

  // const insurance = new Intl.NumberFormat("id-ID", {
  //   style: "currency",
  //   currency: "IDR",
  // }).format(openbid * (50 / 100));

  return (
    <div>
      <Wrapper
        title="Upload Proof of Payment"
        subtitle="Please transfer your payment to 034 101 000 743 303"
        padding="p-6"
      >
        <div className="w-full bg-secondary px-6 py-4 rounded text-sm">
          <p className="font-medium">Amount Due :</p>
          <p className="font-semibold text-lg mt-2">{toRupiah(amountDue)}</p>
        </div>
        <form method="post" onSubmit={handleSubmit}>
          <div className="mt-8">
            <label className="block text-sm font-medium">Upload Photo</label>
            <div className="mt-2 flex justify-center px-6 pt-5 pb-6 border border-gray-300 rounded-md">
              <div className="space-y-1 text-center">
                <div className="mb-4">
                  <img src={picture} className="w-24 mb-2" />
                  <p className="text-xs text-center text-color-secondary">
                    {pictureName}
                  </p>
                </div>
                <div className="flex text-sm text-gray-600 justify-center">
                  <label
                    htmlFor="file-upload"
                    className="relative cursor-pointer bg-white rounded-md font-medium text-primary hover:text-primary-hover focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-primary/50"
                  >
                    <span>Add Image</span>
                    <input
                      id="file-upload"
                      name="file-upload"
                      type="file"
                      className="sr-only"
                      onChange={handleImage}
                    />
                  </label>
                </div>
                {/* <p className="text-xs text-gray-500">
                  PNG, JPG, GIF up to 10MB
                </p> */}
              </div>
            </div>
          </div>
          <div className="mt-16">
            <Button action="Upload" width="w-40" type="submit" />
          </div>
        </form>
      </Wrapper>
    </div>
  );
};

export default PaymentReceiptUpload;

PaymentReceiptUpload.getLayout = (page) => {
  return <Layout title={"Upload Payment Receipt"}>{page}</Layout>;
};
