import React, { useEffect, useState } from "react";
import TablePayment from "../../../../components/Profile/TablePayment";
import Wrapper from "../../../../components/Profile/Wrapper";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import Layout from "../../../../layouts/Profile";
import { paymentRepository } from "../../../../repository/payment";
import { parseJwt } from "../../../../helper/DecodeJwt";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInbox } from "@fortawesome/free-solid-svg-icons";
import { toRupiah } from "../../../../helper/toRupiah";

const Payment = () => {
  useAuthenticationPage();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [id, setId] = useState("");
  const [token, setToken] = useState("");

  useEffect(() => {
    if (typeof window !== "undefined") {
      setToken(localStorage.getItem("token"));
    }
  }, [token]);

  const decodeJwt = parseJwt(token);
  useEffect(() => {
    if (decodeJwt) {
      setId(decodeJwt.result.id);
    }
    // console.log(decodeJwt);
  }, []);

  const { data: paymentData } = paymentRepository.hooks.getPaymentList();

  useEffect(() => {
    if (paymentData !== null && paymentData !== undefined) {
      const tempData = paymentData.data.map((v) => {
        // new Date(v?.purchase_date).toLocaleDateString("id-ID", {
        //   timeZone: "Asia/Jakarta",
        //   hour: "2-digit",
        //   minute: "2-digit",
        // });
        const date = new Date(v?.purchase_date);
        const convertedDate = new Intl.DateTimeFormat("id-ID", {
          timeZone: "Asia/Jakarta",
        }).format(date);

        return {
          id: v?.id,
          transaction: v?.product?.name,
          status:
            v?.status == null || v?.status == "" || v?.status == undefined
              ? "unpaid"
              : v?.status,
          amount_due: toRupiah(v?.fixed_price),
          date: convertedDate,
        };
      });

      setData(tempData);
      setLoading(false);
    }
  }, [paymentData]);
  console.log(paymentData);

  const columns = [
    { Header: "Transaction", accessor: "transaction" },
    { Header: "Status", accessor: "status" },
    { Header: "Amount due", accessor: "amount_due" },
    { Header: "Payment date", accessor: "date" },
  ];
  return (
    <div>
      <Wrapper
        title="Payment"
        subtitle="Here you can see payment data and invoice"
      >
        {loading ? (
          <div className="p-6 text-center">
            <FontAwesomeIcon
              icon={faInbox}
              className="text-3xl text-color-secondary"
            />
            <p className="text-color-secondary mt-4 text-sm">
              There is no payment for now..
            </p>
          </div>
        ) : (
          <TablePayment cols={columns} API={data} />
        )}
      </Wrapper>
    </div>
  );
};

export default Payment;

Payment.getLayout = (page) => {
  return <Layout title="Payment">{page}</Layout>;
};
