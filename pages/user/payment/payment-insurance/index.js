import React, { useEffect, useState } from "react";
import Tabel from "../../../../components/Profile/Tabel";
import TableInsurance from "../../../../components/Profile/TableInsurance";
import Wrapper from "../../../../components/Profile/Wrapper";
import useAuthenticationPage from "../../../../helper/authenticationPage";
import Layout from "../../../../layouts/Profile";
import { paymentRepository } from "../../../../repository/payment";
import { parseJwt } from "../../../../helper/DecodeJwt";
import { toRupiah } from "../../../../helper/toRupiah";

const Insurance = () => {
  useAuthenticationPage();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [id, setId] = useState("");
  const [token, setToken] = useState("");

  useEffect(() => {
    if (typeof window !== "undefined") {
      setToken(localStorage.getItem("token"));
    }
  }, [token]);

  const decodeJwt = parseJwt(token);
  useEffect(() => {
    if (decodeJwt) {
      setId(decodeJwt.result.id);
    }
    // console.log(decodeJwt);
  }, []);

  const { data: paymentData } = paymentRepository.hooks.getInsuranceData(id);

  useEffect(() => {
    if (paymentData !== null && paymentData !== undefined) {
      const tempData = paymentData.data.map((v) => {
        const date = new Date(v?.purchase_date);
        const convertedDate = new Intl.DateTimeFormat("id-ID", {
          timeZone: "Asia/Jakarta",
        }).format(date);

        return {
          id: v?.id,
          transaction: v?.product?.name,
          status: v?.status,
          amount_due: toRupiah(v?.price),
          date: convertedDate,
        };
      });

      setData(tempData);
      setLoading(false);
    }
  }, [paymentData]);

  const columns = [
    { Header: "Transaction", accessor: "transaction" },
    { Header: "Status", accessor: "status" },
    { Header: "Amount due", accessor: "amount_due" },
    { Header: "Payment date", accessor: "date" },
  ];
  return (
    <div>
      <Wrapper
        title="Insurance payment"
        subtitle="Here you can see payment data and invoice"
      >
        {loading ? (
          <div>loading..</div>
        ) : (
          <TableInsurance cols={columns} API={data} />
        )}
      </Wrapper>
    </div>
  );
};

export default Insurance;

Insurance.getLayout = (page) => {
  return <Layout title="Insurance payment">{page}</Layout>;
};
