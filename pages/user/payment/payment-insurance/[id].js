import React, { useEffect, useState } from "react";
import Layout from "../../../../layouts/Profile";
import Image from "next/image";
import { paymentRepository } from "../../../../repository/payment";
import { useRouter } from "next/router";
import { toRupiah } from "../../../../helper/toRupiah";
import Link from "next/link";

const InsuranceDetail = () => {
  const [insurance, setInsurance] = useState([]);
  const router = useRouter();
  const { id } = router.query;

  const { data: insuranceData } = paymentRepository.hooks.getInsuranceById(id);

  useEffect(() => {
    if ((insuranceData !== null) & (insuranceData !== undefined)) {
      setInsurance(insuranceData.data);
    }
  }, [insuranceData]);

  return (
    <>
      <div className="border border-[#e8e8e8] rounded-md px-14 py-6 pb-12">
        <div className="flex justify-between items-center">
          <div>
            <Image src={"/logoLight.svg"} width={"80px"} height={"40px"} />
          </div>
          <div className="uppercase text-base font-medium">{insurance.id}</div>
        </div>
        <div className="my-14 flex justify-between">
          <div className="grid grid-cols-2 gap-y-4 gap-x-6 text-sm w-4/12">
            <div className="font-semibold capitalize">buyers name</div>
            <div className="capitalize">{insurance?.user?.name}</div>
            <div className="font-semibold capitalize">Phone number</div>
            <div>
              {insurance?.user?.phone_number
                ? insurance?.user?.phone_number
                : "-"}
            </div>
            <div className="font-semibold capitalize">address</div>
            <div>{insurance?.user?.addres ? insurance?.user?.addres : "-"}</div>
            <div className="font-semibold capitalize">payment date</div>
            <div>{insurance?.purchase_date}</div>
          </div>
          <div className="grid grid-cols-2 gap-y-4 gap-x-6 text-sm">
            <div className="font-semibold capitalize">Seller</div>
            <div>{insurance?.product?.user?.name}</div>
          </div>
        </div>
        <div className="mt-16">
          <table className="w-full">
            <thead>
              <tr className="text-start font-semibold bg-secondary ">
                <td className="w-10 py-6 px-4">No.</td>
                <td>Product name</td>
                <td>Unit Price</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="w-10 py-6 px-4">1</td>
                <td>{insurance?.product?.name}</td>
                <td>{toRupiah(insurance?.price)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="flex gap-x-6 font-bold justify-end self-end mt-20">
          <p>Total</p>
          <p>{toRupiah(insurance?.price)}</p>
        </div>
      </div>
    </>
  );
};

export default InsuranceDetail;

InsuranceDetail.getLayout = (page) => {
  return <Layout title="Insurance detail">{page}</Layout>;
};
