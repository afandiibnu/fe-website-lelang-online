import React from "react";
import Guest from "../layouts/Guest";

const Faq = () => {
  return (
    <div className="min-h-screen px-64">
      <div className="mt-[100px] flex flex-col items-center">
        <p className="text-3xl font-bold text-center">
          Aero auction <span className="text-primary">FAQ's</span>
        </p>
        <div className="mt-16 grid grid-cols-3 gap-10 ">
          <div className="px-6 py-8 border rounded-md drop-shadow-xl">
            <p className="font-medium text-lg">How to bid?</p>
            <p className="text-color-secondary mt-8 leading-relaxed text-sm">
              Login or register if you don't have an account yet, search for the
              auction item you want to buy. then pay a security deposit if you
              want to participate in the auction. if you win you have to pay the
              rest of the payment and the goods will be sent no later than 3
              days after payment
            </p>
          </div>
          <div className="px-6 py-8 border rounded-md drop-shadow-xl">
            <p className="font-medium text-lg">How to sell?</p>
            <p className="text-color-secondary mt-8 leading-relaxed text-sm">
              Register as an auctioner first on the profile page. you can sell
              art after become an auctioner
            </p>
          </div>
          <div className="px-6 py-8 border rounded-md drop-shadow-xl">
            <p className="font-medium text-lg">How to pay?</p>
            <p className="text-color-secondary mt-8 leading-relaxed text-sm">
              Pay according to the terms and transfer via bank. send to bank
              account number 034 101 000 743 303
            </p>
          </div>
          <div className="px-6 py-8 border rounded-md drop-shadow-xl">
            <p className="font-medium text-lg">
              What if we lose? will my money back?
            </p>
            <p className="text-color-secondary mt-8 leading-relaxed text-sm">
              Yes, 90% of your security deposit will be refunded. 10% of your
              security deposit as handling fee
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Faq;

Faq.getLayout = (page) => {
  return <Guest>{page}</Guest>;
};
