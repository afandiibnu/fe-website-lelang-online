import React from "react";
import Banner from "../components/Banner";
import Guest from "../layouts/Guest";
import { productRepository } from "../repository/product";
import { useState, useEffect } from "react";
import Link from "next/link";
import Typewriter from "typewriter-effect";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { parseJwt } from "../helper/DecodeJwt";

const Search = () => {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [search, setSearch] = useState("");

  const [dataAuction, setDataAuction] = useState([]);

  const { data: auction } = productRepository.hooks.getSearch({
    page,
    limit,
    search,
  });

  useEffect(() => {
    if (auction !== null && auction !== undefined) {
      setDataAuction(auction);
    }
  }, [dataAuction]);

  let token;
  if (typeof window !== "undefined") {
    token = localStorage.getItem("token");
  }
  const decode = parseJwt(token);

  return (
    <div className="min-h-screen">
      <Banner>
        <div className="h-[400px] text-white flex flex-col items-center justify-center">
          <div className="text-4xl font-bold flex">
            <p className=" ">Find</p>
            <span className="text-light-green ml-2">
              <Typewriter
                options={{ autoStart: true, loop: true }}
                onInit={(typewriter) => {
                  typewriter
                    .typeString("Auction")
                    .pauseFor(2000)
                    .deleteAll()
                    .typeString("Digital art")
                    .pauseFor(2000)
                    .deleteAll()
                    .typeString("Fine art")
                    .pauseFor(2000)
                    .start();
                }}
              />
            </span>
          </div>
          <p className="mt-2 text-color-secondary">
            Search auction by name, category, and tags
          </p>
          <div className="relative flex items-center mt-10 gap-x-2">
            <input
              className="bg-white text-black h-10 rounded focus:outline-none px-12 text-sm flex gap-x-2 "
              onChange={(e) => setSearch(e.target.value)}
              placeholder="Search auction..."
            />
            <button
              type="submit"
              className="h-10 px-6 bg-primary rounded text-sm hover:bg-primary-hover"
            >
              Search
            </button>
            <FontAwesomeIcon
              icon={faSearch}
              className="absolute text-color-secondary bottom-3 left-4"
            />
          </div>
        </div>
      </Banner>
      <div className="flex flex-wrap justify-center pt-[100px] pb-[100px] gap-10">
        {auction?.items.map((v) => {
          let status;
          let text;
          let bg;

          const now = new Date();
          const start_date = new Date(v.start_date);
          const end_date = new Date(v.end_date);

          const seller = v?.user?.id;
          const login = decode?.result?.id;

          if (now >= start_date && now <= end_date) {
            status = "ongoing";
            text = "text-green-700";
            bg = "bg-green-100";
          }
          if (now <= start_date) {
            status = "upcoming";
            text = "text-yellow-700";
            bg = "bg-yellow-100";
          }
          if (now >= end_date) {
            status = "Finished";
            text = "text-red-700";
            bg = "bg-red-100";
          }
          if (seller !== login && v?.buyed != true) {
            return (
              <Link href={`/product/categories/${v.category}/${v.id}`}>
                <div
                  className=" bg-white rounded p-2 drop-shadow-2xl cursor-pointer"
                  key={v.id}
                >
                  <div
                    className={`rounded h-60 overflow-hidden min-w-[300px] max-w-[300px] relative`}
                  >
                    <div
                      className={`absolute bottom-2 right-2 py-1 px-3 text-xs rounded-full ${text} ${bg}`}
                    >
                      {status}
                    </div>
                    <img
                      src={`http://localhost:3222/product/get-product-image/${v.picture}`}
                      className="w-full "
                    />
                  </div>
                  <div className="mt-4">
                    <p className="text-xs capitalize text-color-secondary">
                      {v.category}
                    </p>
                    <p className="font-medium capitalize mt-1">{v.name}</p>
                    <p>{}</p>
                  </div>
                </div>
              </Link>
            );
          }
        })}
      </div>
    </div>
  );
};

export default Search;
Search.getLayout = (page) => {
  return <Guest> {page}</Guest>;
};
